const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json');
const fs = require('fs');
const database = require('./database/connect');
const Money = database.import('./models/money');
const cron = require('node-cron');
const Rent = database.import('./models/rent');
const Track = database.import('./models/track');
const Challenges = database.import('./models/challenges');
const ChallengeEarns = database.import('./models/challengeEarns');
const Boost = database.import('./models/boostPotion');
const PurpleGem = database.import('./models/purpleGem');
const RoomBoost = database.import('./models/roomBooster');
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}
const cooldowns = new Discord.Collection();
const blacklist = [ 'ooc', 'character', 'test', 'search', 'chat', 'info', 'information', 'contest'];
const w = 'WWW';
const com = 'COM';
const http = 'HTTP';
const https = 'HTTPS';

cron.schedule('0 0 0 * * * *', async function() {
	const results = await Rent.findAll();
	results.forEach(async result => {
		if (result.days < 0) return;
		result.days = result.days - 1;
		await result.save();
		const searchedChannel = client.channels.get(result.roomID);
		if (!searchedChannel) return;
		if (!result.topic)
		{
			searchedChannel.setTopic(`This room has ${result.days} days left.`);
		}
		else {
			searchedChannel.setTopic(`${result.topic} || This room has ${result.days} days left.`);
		}
		if (result.days === 0)
		{
			const logchannel = client.channels.get(config.rent_log);
			logchannel.send(`❌ <@${result.ownerID}>'s room ${searchedChannel.name} was deleted.`);
			searchedChannel.delete();
		}
		else if (result.days === 1)
		{
			searchedChannel.send(`Dawn of the Final Day, <@${result.ownerID}>! Type /rraddtime ###(number of days) to add more days!`);
		}
	});
});
cron.schedule('0 0 * * 1', async function() {
	const results = await Track.findAll();
	results.forEach(async result => {
		result.weekGems = 0;
		await result.save();
	});
	await recreateTable();
	const randChallenge = config.challenges[Math.floor(Math.random() * config.challenges.length)];
	const randExtChal = config.extendedChallenges[Math.floor(Math.random() * config.extendedChallenges.length)];
	const theChallenge = await Challenges.findOne({ where: { type: 'main' } });
	if(randChallenge.id)
	{
		theChallenge.challengeCat = randChallenge.id;
	}
	else {
		theChallenge.challengeCat = 0;
	}
	theChallenge.challengeName = randChallenge.name;
	if (randChallenge.challengegoal1)
	{
		theChallenge.rank1 = randChallenge.challengegoal1;
		theChallenge.rank2 = randChallenge.challengegoal2;
		theChallenge.rank3 = randChallenge.challengegoal3;
	}
	else {
		theChallenge.rank1 = config.challengegoal1;
		theChallenge.rank2 = config.challengegoal2;
		theChallenge.rank3 = config.challengegoal3;
	}
	await theChallenge.save();
	const theSecondChallenge = await Challenges.findOne({ where: { type: 'secondary' } });
	theSecondChallenge.challengeCat = randExtChal.id;
	theSecondChallenge.challengeName = randExtChal.name;
	theSecondChallenge.rank1 = config.challengegoal1,
	theSecondChallenge.rank2 = config.challengegoal2;
	theSecondChallenge.rank3 = config.challengegoal3;
	await theSecondChallenge.save();
	const main = client.channels.get('279584314856046592');
	main.send(`The Challenge log has been updated! ${randChallenge.rank1} and ${randExtChal.rank1} are now available!`);
});
client.login(config.token);
client.on('ready', () => {
	console.log(`Arita has been booted up! Arita operates in ${client.guilds.size} servers, and for ${client.users.size} people!`);
});
client.on('error', (e) => console.error(e));
client.on('warn', (e) => console.warn(e));
client.on('debug', (e) => console.info(e));
process.on('unhandledRejection', console.error);
client.on('messageDelete', async(messageDelete) => {
	if (config.rp_subchannel.includes(messageDelete.channel.parentID) && (!messageDelete.content.startsWith('(')))
	{
		const rem = messageDelete.content.split(' ').length * 0.02;
		const user = await Money.findOne({ where: { userId: messageDelete.author.id } });
		user.decrement('gems', { by: rem });
		const user2 = await Boost.findOne({ where: { userId: messageDelete.author.id } });
		if (!user2) return;
		const total = rem * user2.multiplier;
		user.decrement('gems', { by: total });
		user2.increment('time', { by: 1 });
	}
});
client.on('message', async message =>{
	if (message.content.startsWith(config.prefix + 'eval')) {
		if(message.author.id !== config.ownerID) return;
		const args = message.content.split(' ').slice(1);
		try {
			message.delete();
			const code = args.join(' ');
			let evaled = eval(code);
			// if (typeof evaled !== 'string')
			evaled = require('util').inspect(evaled);
			message.channel.send(clean(evaled), { code:'xl' });
		} catch (err) {
			message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
		}
	}

	else if (message.channel.id == config.main_chat || message.channel.id == config.lewd_chat || message.channel.id == config.conversation_chat)
	{
		const msgArray = message.content.split(' ');
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: config.general_gain });
		}
		tracker(message, config.general_gain);
		challenge(message, config.general_gain);
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) return;

		for(let i = 0; i < msgArray.length; i++) {
			const index = msgArray[i].toUpperCase();
			if(index.indexOf(w + '.') !== -1 && index.indexOf('.' + com) !== -1) {
				message.delete();
				message.author.send(`Arita's words echo through your mind..."Hey! ${message.author}! No links allowed in main!"`);
			}
			else if(index.indexOf(http) !== -1 || index.indexOf(https) !== -1) {
				if(index.indexOf(w + '.') || index.indexOf('.' + com)) {
					message.delete();
					message.author.send(`Arita's words echo through your mind..."Hey! ${message.author}! No links allowed in main!"`);
				}
			}
		}
	}
	else if (config.image_subchannel.includes(message.channel.parentID) && (message.attachments.size) || (config.image_subchannel.includes(message.channel.parentID)) && (message.content.startsWith('https://'))) {
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: config.image_gain });
		}
		tracker(message, config.image_gain);
		challenge(message, config.image_gain);
	}
	else if (config.rp_subchannel.includes(message.channel.parentID) && (!message.content.startsWith('(')))
	{
		if (message.content.startsWith(config.prefix)) return commands(message);
		let loopOver = false;
		for(let i = 0; i < blacklist.length; i++) {
			if(message.channel.name.includes(blacklist[i]))
			{
				loopOver = true;
				return;
			}
		}
		if (loopOver) return;
		const count = message.content.split(' ').length;
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		const moneyTotal = count * config.rp_word_gain;
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: moneyTotal });
		}
		tracker(message, moneyTotal);
		challenge(message, moneyTotal);
		gemPotion(message, userName, moneyTotal);
		roomBoost(message, userName, moneyTotal);
	}
	else if (config.casual_subchannel.includes(message.channel.parentID))
	{
		let loopOver = false;
		for(let i = 0; i < blacklist.length; i++) {
			if(message.channel.name.includes(blacklist[i]))
			{
				loopOver = true;
				return;
			}
		}
		if (loopOver) return;
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: config.varied_gain });
		}
		tracker(message, config.varied_gain);
		challenge(message, config.varied_gain);
	}
	else if (message.channel.parentID == config.staff || message.channel.parentID == config.operators)
	{
		if (message.content.startsWith(config.prefix)) return commands(message);
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		if (!userName) {
			try {
				await Money.create({
					userId: message.author.id,
					gems: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		else {
			userName.increment('gems', { by: config.staff_gain });
		}
		tracker(message, config.staff_gain);
	}
	else {
		if (!message.content.startsWith(config.prefix) || message.author.bot) return;
		commands(message);
	}
});

function commands(message) {
	const args = message.content.slice(config.prefix.length).split(/ +/);
	const commandName = args.shift().toLowerCase();

	const command = client.commands.get(commandName)
	|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
	if (!command) return;
	if (command.args && !args.length) {
		let reply = message.channel.send(`You haven't provided any arguments, ${message.author}!`);

		if (command.usage) {
			reply += `\nThe proper usage of this command would be: \`${config.prefix}${command.name} ${command.usage}\``;
		}
		return message.channel.send(reply);
	}
	if(!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}
	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 3) * 1000;
	if (!timestamps.has(message.author.id)) {
		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	}
	else{
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;
		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`please wait at least ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command!`);
		}
		timestamps.set(message.author.id, now);
		setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	}
	try{
		command.execute(message, args);
	}
	catch (error) {
		console.error(error);
		message.reply('something has gone wrong when executing that command!');
	}
}

function clean(text) {
	if (typeof (text) === 'string')
	{return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));}
	else{
		return text;
	}
}

async function tracker(message, earnings) {
	const channelCat = await Track.findOne({ where: { catId: message.channel.parentID } });
	if (!channelCat) {
		try {
			await Track.create({
				catId: message.channel.parentID,
				catName: message.channel.parent.name,
				gems: earnings,
				weekGems: earnings,
			});
		}
		catch(e) {
			return console.log(e);
		}
	}
	else {
		channelCat.increment('gems', { by: earnings });
		channelCat.increment('weekGems', { by: earnings });
	}

}

async function challenge(message, earnings) {
	const activeCat = await Challenges.findAll();
	if (!activeCat) return;
	activeCat.forEach(async active => {
		if (active.challengeCat == '0') {
			const user = await ChallengeEarns.findOne({ where: { userId: message.author.id } });
			if (!user) {
				try {
					await ChallengeEarns.create({
						userId: message.author.id,
						challengeGems: earnings,
						challengeClear: 0,
						extendedGems: 0,
						extendedClear: 0,
					});
				}
				catch(e) {
					return console.log(e);
				}
				return;
			}
			rewards(message, user, active, earnings);
			extendedRewards(message, user, active, earnings);
		}
		else {
			if (active.challengeCat != message.channel.parentID) return;
			const user = await ChallengeEarns.findOne({ where: { userId: message.author.id } });
			if (!user) {
				try {
					await ChallengeEarns.create({
						userId: message.author.id,
						challengeGems: earnings,
						challengeClear: 0,
						extendedGems: 0,
						extendedClear: 0,
					});
				}
				catch(e) {
					return console.log(e);
				}
				return;
			}
			rewards(message, user, active, earnings);
			extendedRewards(message, user, active, earnings);
		}
	});
}

async function rewards(message, user, active, earnings)
{
	if (active.type == 'secondary') return;
	const userGems = user.challengeGems + earnings;
	user.increment('challengeGems', { by: earnings });
	await user.save().then(async() => {
		if (user.challengeClear == 0 && userGems >= active.rank1) {
			user.increment('challengeClear', { by: 1 });
			await user.save();
			const catInfo = await keyFinder(active, config.challenges);
			message.channel.send(`The challenge ${catInfo.rank1} has been completed!`);
			giveMoney(message, user, 0.0025, 5);
			giveDarkness(message, 1);
		}
		else if (user.challengeClear == 1 && userGems >= active.rank2) {
			user.increment('challengeClear', { by: 1 });
			await user.save();
			const catInfo = await keyFinder(active, config.challenges);
			message.channel.send(`The challenge ${catInfo.rank2} has been completed!`);
			giveMoney(message, user, 0.005, 10);
			giveDarkness(message, 2);
		}
		else if (user.challengeClear == 2 && userGems >= active.rank3) {
			user.increment('challengeClear', { by: 1 });
			await user.save();
			const catInfo = await keyFinder(active, config.challenges);
			message.channel.send(`The challenge ${catInfo.rank3} has been completed!`);
			giveMoney(message, user, 0.01, 20);
			giveDarkness(message, 3);
		}
	});

}

async function extendedRewards(message, user, active, earnings)
{
	if (active.type == 'main') return;
	const userGems = user.extendedGems + earnings;
	user.increment('extendedGems', { by: earnings });
	await user.save().then(async() => {
		if (user.extendedClear == 0 && userGems >= active.rank1) {
			user.increment('extendedClear', { by: 1 });
			await user.save();
			const catInfo = await keyFinder(active, config.extendedChallenges);
			message.channel.send(`The challenge ${catInfo.rank1} has been completed!`);
			giveMoney(message, user, 0.0025, 5);
			giveDarkness(message, 1);
		}
		else if (user.extendedClear == 1 && userGems >= active.rank2) {
			user.increment('extendedClear', { by: 1 });
			await user.save();
			const catInfo = await keyFinder(active, config.extendedChallenges);
			message.channel.send(`The challenge ${catInfo.rank2} has been completed!`);
			giveMoney(message, user, 0.005, 10);
			giveDarkness(message, 2);
		}
		else if (user.extendedClear == 2 && userGems >= active.rank3) {
			user.increment('extendedClear', { by: 1 });
			await user.save();
			const catInfo = await keyFinder(active, config.extendedChallenges);
			message.channel.send(`The challenge ${catInfo.rank3} has been completed!`);
			giveMoney(message, user, 0.01, 20);
			giveDarkness(message, 3);
		}
	});
}

async function keyFinder(activeCat, whichType) {
	const challengeInConfig = whichType.find(obj => obj.name == activeCat.challengeName);
	return challengeInConfig;
}

async function giveMoney(message, user, reward, bonus) {
	for (let i = 0; i <= config.role.length; i++) {
		if (!message.member.roles.has(config.role[i])) continue;
		const userName = await Money.findOne({ where: { userId: message.author.id } });
		const percent = reward * config.upgradePrice[i];
		const total = percent + bonus;
		userName.increment('gems', { by: total });
	}
}

async function giveDarkness(message, active) {
	let user = await PurpleGem.findOne({ where: { userID: message.author.id } });
	if (!user) {
		try {
			await PurpleGem.create({
				userID: message.author.id,
				darkness: 0,
				purpleGem: 0,
			});
			user = await PurpleGem.findOne({ where: { userID: message.author.id } });
			if (!user) return message.reply('something went wrong!');
		}
		catch(e) {
			return console.log(e);
		}
	}
	if (active == 1) {
		const darkNumber = parseInt(user.darkness, 10);
		const total = darkNumber + 10;
		user.increment('darkness', { by: 25 });
		if (total > 99) {
			user.darkness = 0;
			await user.save();
			user.increment('purpleGem', { by: 1 });
			return message.reply('after obtaining enough darkness, you start getting to know your dark side a bit better, you get a Purple Gem as a result.');
		}
		return;
	}
	if (active == 2) {
		const total = user.darkness + 50;
		user.increment('darkness', { by: 50 });
		if (total > 99) {
			user.darkness = 0;
			await user.save();
			user.increment('purpleGem', { by: 1 });
			return message.reply('after obtaining enough darkness, you start getting to know your dark side a bit better, you get a Purple Gem as a result.');
		}
		return;
	}
	if (active == 3) {
		user.increment('purpleGem', { by: 1 });
		return;
	}
}

async function recreateTable() {
	await database.getQueryInterface().dropTable('challengeEarns');
	await database.getQueryInterface().createTable(
		'challengeEarns', {
			userId: {
				type: database.Sequelize.STRING,
				primaryKey: true,
			},
			challengeGems: database.Sequelize.FLOAT,
			challengeClear: database.Sequelize.INTEGER,
			extendedGems: database.Sequelize.FLOAT,
			extendedClear: database.Sequelize.INTEGER,
		});
}


async function gemPotion(message, userName, moneyTotal) {
	const user = await Boost.findOne({ where: { userId: message.author.id } });
	if (!user) return;
	const total = moneyTotal * user.multiplier;
	userName.increment('gems', { by: total });
	const postsLeft = user.time - 1;
	user.decrement('time', { by: 1 });
	if (postsLeft < 1) {
		if (user.type == 'high') {
			user.time = 25;
			user.type = 'withdrawal';
			user.multiplier = -0.50;
			await user.save();
		}
		else {
			user.destroy();
			message.channel.send('You no longer feel the effects of the potion on you.');
		}
	}
}


async function roomBoost(message, userName, moneyTotal) {
	const room = await RoomBoost.findOne({ where: { roomId: message.channel.id } });
	if (!room) return;
	const totalGems = moneyTotal * room.multiplier;
	userName.increment('gems', { by: totalGems });
	const postsLeft = room.time - 1;
	room.decrement('time', { by: 1 });
	if (!postsLeft < 1) return;
	room.destroy();
	message.channel.send('the Cuddlefish blessing quietly fades from this room.');
}
