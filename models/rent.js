module.exports = (sequelize, DataTypes) => {
	return sequelize.define('rent', {
		ownerID: DataTypes.STRING,
		roomID: {
			type: DataTypes.STRING,
			unique: true,
		},
		days: DataTypes.INTEGER,
		topic: DataTypes.STRING,
	});
};
