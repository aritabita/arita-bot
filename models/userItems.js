module.exports = (sequelize, DataTypes) => {
	return sequelize.define('userItems', {
		userId: DataTypes.STRING,
		itemKey : DataTypes.STRING,
		amount: DataTypes.INTEGER,
	});
};
