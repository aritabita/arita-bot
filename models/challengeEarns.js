module.exports = (sequelize, DataTypes) => {
	return sequelize.define('challengeEarns', {
		userId: {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		challengeGems: DataTypes.FLOAT,
		challengeClear: DataTypes.INTEGER,
		extendedGems: DataTypes.FLOAT,
		extendedClear: DataTypes.INTEGER,
	},
	{
		timestamps: false,
	});
};
