module.exports = (sequelize, DataTypes) => {
	return sequelize.define('track', {
		catId: {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		catName: DataTypes.STRING,
		gems: DataTypes.FLOAT,
		weekGems: DataTypes.FLOAT,
	},
	{
		timestamps: false,
	});
};
