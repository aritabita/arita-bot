module.exports = (sequelize, DataTypes) => {
	return sequelize.define('anniversaryCurrency', {
		userId : {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		bubbles: DataTypes.INTEGER,
		spinach: DataTypes.INTEGER,
		bits: DataTypes.INTEGER,
		tentacles: DataTypes.INTEGER,
		wahCoins: DataTypes.INTEGER,
	});
};
