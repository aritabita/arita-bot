module.exports = (sequelize, DataTypes) => {
	return sequelize.define('money', {
		userId: {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		gems: DataTypes.FLOAT,
	},
	{
		timestamps: false,
	});
};
