module.exports = (sequelize, DataTypes) => {
	return sequelize.define('gift', {
		userId: {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		gift: DataTypes.INTEGER,
		exp: DataTypes.FLOAT,
		level: DataTypes.INTEGER,
	});
};
