module.exports = (sequelize, DataTypes) => {
	return sequelize.define('boostPotion', {
		userId: DataTypes.STRING,
		type: DataTypes.STRING,
		multiplier: DataTypes.FLOAT,
		time: DataTypes.INTEGER,
	});
};
