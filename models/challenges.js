module.exports = (sequelize, DataTypes) => {
	return sequelize.define('challenges', {
		type: {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		challengeCat: DataTypes.STRING,
		challengeName: DataTypes.STRING,
		rank1: DataTypes.INTEGER,
		rank2: DataTypes.INTEGER,
		rank3: DataTypes.INTEGER,
	});
};
