module.exports = (sequelize, DataTypes) => {
	return sequelize.define('purpleGem', {
		userID: {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		darkness: DataTypes.STRING,
		purpleGem: DataTypes.STRING,
	});
};
