module.exports = (sequelize, DataTypes) => {
	return sequelize.define('candy', {
		userId : {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		candy: DataTypes.INTEGER,
		exp: DataTypes.FLOAT,
		level: DataTypes.INTEGER,
		foxMask: DataTypes.INTEGER,
		ghostCloth: DataTypes.INTEGER,
		lostFigurine: DataTypes.INTEGER,
		abandonedTie: DataTypes.INTEGER,
		wyvernTail: DataTypes.INTEGER,
		sparkler: DataTypes.INTEGER,
		shapeshifterHair: DataTypes.INTEGER,
		unidentifiableGenus: DataTypes.INTEGER,
		dinosaurFang: DataTypes.INTEGER,
		pandaMask: DataTypes.INTEGER,
		pranksterSoul: DataTypes.INTEGER,
		empressCrown: DataTypes.INTEGER,
	});
};
