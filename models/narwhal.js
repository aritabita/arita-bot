module.exports = (sequelize, DataTypes) =>{
	return sequelize.define('narwhal', {
		team: {
			type: DataTypes.STRING,
			unique: true,
		},
		points: DataTypes.FLOAT,
	});
};
