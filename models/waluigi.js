module.exports = (sequelize, DataTypes) => {
	return sequelize.define('waluigi', {
		userId: {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		transformation: DataTypes.INTEGER,
	});
};
