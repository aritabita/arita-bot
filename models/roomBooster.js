module.exports = (sequelize, DataTypes) => {
	return sequelize.define('roomBooster', {
		roomId : {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		multiplier: DataTypes.FLOAT,
		time: DataTypes.INTEGER,
	});
};
