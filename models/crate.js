module.exports = (sequelize, DataTypes) => {
	return sequelize.define('crate', {
		key: {
			type: DataTypes.STRING,
			primaryKey: true,
		},
		name: DataTypes.STRING,
		rarity: DataTypes.STRING,
		description: DataTypes.STRING,
	});
};
