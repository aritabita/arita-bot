module.exports = (sequelize, DataTypes) => {
	return sequelize.define('cardInventory', {
		userId: DataTypes.STRING,
		cardKey: DataTypes.STRING,
		amount: DataTypes.INTEGER,
	});
};
