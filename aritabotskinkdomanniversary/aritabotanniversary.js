const Anniversary = database.import('./models/anniversaryCurrency');
// To call in categories
async function fiendCurrency(message) {
	if (!message.content.toLowerCase().includes('f')) return;
	const randomNumber = Math.floor(Math.random() * 101);
	if (randomNumber >= 3) return;
	let user = await Anniversary.findOne({ where: { userId: message.author.id } });
	if (!user)
	{
		try {
			await Anniversary.create({
				userId: message.author.id,
				bubbles: 0,
				spinach: 0,
				bits: 0,
				tentacles: 0,
				wahCoins: 0,
			});
		}
		catch(e) {
			return console.log(e);
		}
		user = await Anniversary.findOne({ where: { userId: message.author.id } });
	}
	user.increment('tentacles', { by: 1 });
	message.author.send('Attracted by the sounds of something familiar, a lost tentacle crawls up your leg and into your pocket, maybe you could lead it home?').catch(function() {
		return;
	});
}

async function talCurrency(message) {
	let randomCheck = 2;
	if (message.channel.parent.name == 'Chat') randomCheck = 1;
	const randomNumber = Math.floor(Math.random() * 101);
	if (!randomNumber < randomCheck) return;
	let user = await Anniversary.findOne({ where: { userId: message.author.id } });
	if (!user)
	{
		try {
			await Anniversary.create({
				userId: message.author.id,
				bubbles: 0,
				spinach: 0,
				bits: 0,
				tentacles: 0,
				wahCoins: 0,
			});
		}
		catch(e) {
			return console.log(e);
		}
		user = await Anniversary.findOne({ where: { userId: message.author.id } });
	}
	user.increment('spinach', { by: 1 });
	message.author.send('During your casual stroll through the server, you feel something squish under your foot, and backing up you see a pile of green spinach. Pretty gross, actually. But you\'re certain it has a use somewhere...').catch(function() {
		return;
	});
}

async function keelCurrency(message) {
	const randomNumber = Math.floor(Math.random() * 101);
	if (randomNumber != 1) return;
	let user = await Anniversary.findOne({ where: { userId: message.author.id } });
	if (!user)
	{
		try {
			await Anniversary.create({
				userId: message.author.id,
				bubbles: 0,
				spinach: 0,
				bits: 0,
				tentacles: 0,
				wahCoins: 0,
			});
		}
		catch(e) {
			return console.log(e);
		}
		user = await Anniversary.findOne({ where: { userId: message.author.id } });
	}
	user.increment('bits', { by: 1 });
	message.author.send('Looking down, you find a strange golden coin on the floor, a sun on one side and a moon on the other').catch(function() {
		return;
	});
}
async function allyCurrency(message) {
	if (!message.content.toLowerCase().includes('l')) return;
	const randomNumber = Math.floor(Math.random() * 101);
	if (randomNumber >= 2) return;
	let user = await Anniversary.findOne({ where: { userId: message.author.id } });
	if (!user)
	{
		try {
			await Anniversary.create({
				userId: message.author.id,
				bubbles: 0,
				spinach: 0,
				bits: 0,
				tentacles: 0,
				wahCoins: 0,
			});
		}
		catch(e) {
			return console.log(e);
		}
		user = await Anniversary.findOne({ where: { userId: message.author.id } });
	}
	user.increment('bubbles', { by: 1 });
	message.author.send('*A small woman swims over to the edge of her pond and pokes her head out, holding her hands open for you. Inside them, you see she is cradling a small, crystal bubble, and she is offering it to you.* "You\'ve earned this!"').catch(function() {
		return;
	});
}

async function sparkoCurrency(message) {
	if (message.content.includes('<:Whalecum:397017519308931082>') || (message.content.includes('<:WhalecumOfficially:532936896024805382>'))) {
		let user = await Anniversary.findOne({ where: { userId: message.author.id } });
		if (!user)
		{
			try {
				await Anniversary.create({
					userId: message.author.id,
					bubbles: 0,
					spinach: 0,
					bits: 0,
					tentacles: 0,
					wahCoins: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Anniversary.findOne({ where: { userId: message.author.id } });
		}
		user.increment('wahCoins', { by: 1 });
		message.author.send('WAHAHAHA! YOU FOUND A WAHCOIN!').catch(function() {
			return;
		});
	}
}
