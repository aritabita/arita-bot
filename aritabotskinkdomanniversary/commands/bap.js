const database = require('../database/connect');
const Anniversary = database.import('../models/anniversaryCurrency');
module.exports = {
	name: 'bap',
	aliases: ['bp'],
	usage: '<type of currency>',
	description: 'Obtains one of the specified event currency. Types accepted are: ally, tal, fiend, keel and sparko',
	forUser: true,
	cooldown: 28800,
	async execute(message, args) {
		if (message.channel.type !== 'text') return message.channel.send('Can\'t bap from far away!');
		message.delete();
		let user = await Anniversary.findOne({ where: { userId: message.author.id } });
		if (!user) {
			try {
				await Anniversary.create({
					userId: message.author.id,
					bubbles: 0,
					spinach: 0,
					bits: 0,
					tentacles: 0,
					wahCoins: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Anniversary.findOne({ where: { userId: message.author.id } });
		}
		if (!user) return console.log('no user');
		if (args[0] == 'ally') {
			user.increment('bubbles', { by: 1 });
			return message.reply('The goldfish gasps in shock as you bap her, a small bubble escaping her lips with the sound of "blep!"');
		}
		else if (args[0] == 'tal') {
			user.increment('spinach', { by: 1 });
			return message.reply('after bapping a grocery store owner, he angrily gives you a can of spinach, before kicking you out of the store');
		}
		else if (args[0] == 'fiend') {
			user.increment('tentacles', { by: 1 });
			return message.reply('you bap a tentacle until you convince it to come with you.');
		}
		else if (args[0] == 'keel') {
			user.increment('bits', { by: 1 });
			return message.reply('you bap a magical horse until it throws a gold coin in your face');
		}
		else if (args[0] == 'sparko') {
			user.increment('wahCoins', { by: 1 });
			return message.reply('you bap a Waluigi plush until it wahs back at you, and you find a weird coin by it. He\'s still not in Smash though.');
		}
		else {
			const random = Math.floor(Math.random() * 5);
			if (random == 0) user.increment('bubbles', { by: 1 });
			if (random == 1) user.increment('spinach', { by: 1 });
			if (random == 2) user.increment('tentacles', { by: 1 });
			if (random == 3) user.increment('bits', { by: 1 });
			if (random == 4) user.increment('wahCoins', { by: 1 });
			return message.reply('you bap nothing in particular. You still get something, weirdly enough');
		}
	},
};
