const config = require('../../config.json');
const database = require('../../database/connect');
const Item = database.import('../../models/userItems');
module.exports = {
	name: 'cs',
	async execute(message, userItem) {
		const number1 = Math.floor(Math.random() * config.allyAdjectives.length);
		const number2 = Math.floor(Math.random() * config.allyNouns.length);
		const adjective = config.allyAdjectives[number1];
		const noun = config.allyNouns[number2];
		let targetUser;
		let mentionedUser;
		if (!message.mentions.users.first()) {
			mentionedUser = message.guild.members.get(message.author.id);
		}
		else {
			targetUser = message.mentions.users.first();
			mentionedUser = message.guild.members.get(message.mentions.users.first().id);
			if (mentionedUser.roles.has(config.adminID)) return message.reply('the target was unaffected by your attempts!');
			const sweater = await Item.findOne({ where: { userId:targetUser.id, itemKey: 'cc' } });
			if (sweater && sweater.amount > 0) {
				sweater.decrement('amount', { by: 1 });
				mentionedUser = message.guild.members.get(message.author.id);
			}
		}
		mentionedUser.setNickname(`${noun} ${adjective}`);
		const reply = 'As the target takes a deep sniff at this strange potion, they feel as if they float in water, and are one with the fishes, causing them to lose all sense of self. They also feel pretty loving.';
		userItem.decrement('amount', { by: 1 });
		message.reply(reply);
	},
};
