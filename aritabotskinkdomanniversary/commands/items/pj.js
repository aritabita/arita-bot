const config = require('../../config.json');
const database = require('../../database/connect');
const Item = database.import('../../models/userItems');
module.exports = {
	name: 'pj',
	async execute(message, userItem) {
		const type = Math.floor(Math.random() * 2);
		if (type == 0) {
			const number1 = Math.floor(Math.random() * config.keelAdjectives.length);
			const number2 = Math.floor(Math.random() * config.keelNouns.length);
			const adjective = config.keelAdjectives[number1];
			const noun = config.keelNouns[number2];
			let targetUser;
			let mentionedUser;
			if (!message.mentions.users.first()) {
				mentionedUser = message.guild.members.get(message.author.id);
			}
			else {
				targetUser = message.mentions.users.first();
				mentionedUser = message.guild.members.get(message.mentions.users.first().id);
				if (mentionedUser.roles.has(config.adminID)) return message.reply('the target was unaffected by your attempts!');
				const sweater = await Item.findOne({ where: { userId:targetUser.id, itemKey: 'cc' } });
				if (sweater && sweater.amount > 0) {
					sweater.decrement('amount', { by: 1 });
					mentionedUser = message.guild.members.get(message.author.id);
				}
			}
			mentionedUser.setNickname(`${adjective} ${noun}`);
			const reply = 'the poison from the Poison Joke clouds the helpless victim\'s mind, causing them to lose all sense of self. User just got pranked.';
			userItem.decrement('amount', { by: 1 });
			return message.reply(reply);
		}
		else {
			const chosenRole = Math.floor(Math.random() * config.keelRoles.length);
			let targetUser;
			let mentionedUser;
			if (!message.mentions.users.first()) {
				mentionedUser = message.guild.members.get(message.author.id);
			}
			else {
				targetUser = message.mentions.users.first();
				mentionedUser = message.guild.members.get(message.mentions.users.first().id);
				if (mentionedUser.roles.has(config.adminID)) return message.reply('the target was unaffected by your attempts!');
				const sweater = await Item.findOne({ where: { userId:targetUser.id, itemKey: 'cc' } });
				if (sweater && sweater.amount > 0) {
					sweater.decrement('amount', { by: 1 });
					mentionedUser = message.guild.members.get(message.author.id);
				}
			}
			for (let i = 0; i < config.keelRoles.length; i++) {
				mentionedUser.roles.remove(config.keelRoles[i]);
			}
			mentionedUser.roles.add(config.keelRoles[chosenRole]);
		}
	},
};
