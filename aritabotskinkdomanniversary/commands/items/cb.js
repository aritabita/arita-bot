const database = require('../../database/connect');
const RoomBoost = database.import('../../models/roomBooster');
module.exports = {
	name: 'cb',
	async execute(message, userItem) {
		if (message.channel.parentID == '361270100063551490' || message.channel.parentID == '361270217315319819') return;
		message.delete();
		let room = await RoomBoost.findOne({ where: { roomId: message.channel.id } });
		if (!room) {
			try {
				await RoomBoost.create({
					roomId: message.channel.id,
					multiplier: 0.50,
					time: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			room = await RoomBoost.findOne({ where: { roomId: message.channel.id } });
		}
		room.increment('time', { by: 60 });
		message.reply('the room is coated in the Cuddlefish\'s blessing, boosting gem yiels within it temporarily!');
		userItem.decrement('amount', { by: 1 });
	},
};
