const database = require('../../database/connect');
const Boost = database.import('../../models/boostPotion');
module.exports = {
	name: 'fh',
	async execute(message, userItem) {
		const user = await Boost.findOne({ where: { userId: message.author.id } });
		if (!user) return message.reply('the hourglass only reacts to users with a potion in effect!');
		user.increment('time', { by: 20 });
		message.reply('the hourglass shines and disappears, its essence going inside you. You bend time slightly as a result.');
		userItem.decrement('amount', { by: 1 });
	},
};
