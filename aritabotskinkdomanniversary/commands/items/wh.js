const config = require('../../config.json');
module.exports = {
	name: 'wh',
	async execute(message, userItem) {
		let mentionedUser;
		if (!message.mentions.users.first()) mentionedUser = message.guild.members.get(message.author.id);
		else mentionedUser = message.guild.members.get(message.mentions.users.first().id);
		const random = Math.floor(Math.random() * config.sparkoNames.length);
		mentionedUser.setNickname(`${config.sparkoNames[random]}`);
		message.reply('WAH! You have become as great at him! //Fix those stupid typos!!!');
		userItem.decrement('amount', { by: 1 });
	},
};
