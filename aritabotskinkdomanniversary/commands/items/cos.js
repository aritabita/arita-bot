const database = require('../../database/connect');
const Money = database.import('../../models/money');
module.exports = {
	name: 'cos',
	async execute(message, userItem) {
		const user = await Money.findOne({ where: { userId: message.author.id } });
		if (!user) return message.reply('error.');
		if (message.mentions.users.first()) return message.reply('this only works on yourself!');
		user.increment('gems', { by: 5 });
		message.reply('you eat the spinach within the can, and you feel stronger! You also find a few gems in the can. For flavor, apparently?');
		userItem.decrement('amount', { by: 1 });
	},
};
