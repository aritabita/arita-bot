const config = require('../../config.json');
module.exports = {
	name: 'sc',
	async execute(message, userItem) {
		let mentionedUser;
		if (!message.mentions.users.first()) mentionedUser = message.guild.members.get(message.author.id);
		else mentionedUser = message.guild.members.get(message.mentions.users.first().id);
		const randomRole = Math.floor(Math.random() * config.fiendRoles.length);
		for (let i = 0; i < config.fiendRoles.length; i++) {
			mentionedUser.roles.remove(config.fiendRoles[i]);
		}
		mentionedUser.roles.remove(config.sparkoRole);
		mentionedUser.roles.add(config.fiendRoles[randomRole]);
		message.reply('as the coin shifts, its target now becomes afflicted by its curse, or is it a blessing? Either way, the target looks...different, somehow.');
		userItem.decrement('amount', { by: 1 });
	},
};
