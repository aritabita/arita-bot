const database = require('../../database/connect');
const Money = database.import('../../models/money');
module.exports = {
	name: 'pc',
	async execute(message, userItem) {
		if (!message.mentions.users.first()) return message.reply('you can\'t enjoy this nice meal by yourself!');
		if (message.mentions.users.first().id == message.author.id) return message.reply('you cannot share this with yourself!');
		const targetUser = await Money.findOne({ where: { userId: message.mentions.members.first().id } });
		if (!targetUser) return message.reply('something went wrong. Tell Arita to check this item.');
		const user = await Money.findOne({ where: { userId: message.author.id } });
		if (!user) return message.reply('error.');
		targetUser.increment('gems', { by: 5 });
		user.increment('gems', { by: 5 });
		userItem.decrement('amount', { by: 1 });
		message.reply('together, you eat the giantic Plump Cucumber, and within it, you find 10 gems! You split it, trying to not dwell too much on gems being inside food.');
	},
};
