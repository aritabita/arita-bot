const config = require('../../config.json');
const database = require('../../database/connect');
const Item = database.import('../../models/userItems');
module.exports = {
	name: 'op',
	async execute(message, userItem) {
		const number1 = Math.floor(Math.random() * config.talAdjectives.length);
		const number2 = Math.floor(Math.random() * config.talNouns.length);
		const adjective = config.talAdjectives[number1];
		const noun = config.talNouns[number2];
		let targetUser;
		let mentionedUser;
		if (!message.mentions.users.first()) {
			mentionedUser = message.guild.members.get(message.author.id);
		}
		else {
			targetUser = message.mentions.users.first();
			mentionedUser = message.guild.members.get(message.mentions.users.first().id);
			if (mentionedUser.roles.has(config.adminID)) return message.reply('the target was unaffected by your attempts!');
			const sweater = await Item.findOne({ where: { userId:targetUser.id, itemKey: 'cc' } });
			if (sweater && sweater.amount > 0) {
				sweater.decrement('amount', { by: 1 });
				mentionedUser = message.guild.members.get(message.author.id);
			}
		}
		mentionedUser.setNickname(`${adjective} ${noun}`);
		const reply = 'as the target stares onto this little yellow bear figure, they feel transported into a strange world with friendly animals, and they lose their sense of self in the process. They also crave honey.';
		userItem.decrement('amount', { by: 1 });
		message.reply(reply);
	},
};
