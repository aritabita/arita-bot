const database = require('../../database/connect');
const PurpleGem = database.import('../../models/purpleGem');
module.exports = {
	name: 'sog',
	async execute(message, userItem) {
		let user = await PurpleGem.findOne({ where: { userID: message.author.id } });
		if (!user) {
			try {
				await PurpleGem.create({
					userID: message.author.id,
					darkness: 0,
					purpleGem: 0,
				});
				user = await PurpleGem.findOne({ where: { userID: message.author.id } });
			}
			catch(e) {
				return console.log(e);
			}
		}
		if (!user) return message.reply('something went wrong!');
		user.increment('purpleGem', { by: 1 });
		message.reply('under the shadow of the Giant, you find a Purple Gem.');
		userItem.decrement('amount', { by: 1 });
	},
};
