const database = require('../../database/connect');
const Boost = database.import('../../models/boostPotion');
module.exports = {
	name: 'soh',
	async execute(message, userItem) {
		if (!message.mentions.users.first()) return message.reply('you need to use this item with someone else!');
		const target = message.mentions.users.first().id;
		const targetUser = await Boost.findOne({ where: { userId: target } });
		if (targetUser) return message.reply('that user is already under the effects of a potion!');
		const user = await Boost.findOne({ where: { userId: message.author.id } });
		if (user) return message.reply('you cannot use this item while you\'re expreicning a gem boost yourself!');
		try {
			await Boost.create({
				userId: target,
				type: 'rush',
				multipler: 0.50,
				time: 30,
			});
		}
		catch(e) {
			return console.log(e);
		}
		try {
			await Boost.create({
				userId: message.author.id,
				type: 'rush',
				multiplier: 0.50,
				time: 30,
			});
		}
		catch(e) {
			return console.log(e);
		}
		message.reply('the Shard of Harmony glows brightly, and both you and your friend feel a strong bond of friendship connecting you two, boosting your gem gains.');
		userItem.decrement('amount', { by: 1 });
	},
};
