const database = require('../../database/connect');
const Money = database.import('../../models/money');
module.exports = {
	name: 'tc',
	async execute(message, userItem) {
		const user = await Money.findOne({ where: { userId: message.author.id } });
		if (!user) return message.reply('error.');
		if (message.mentions.users.first()) return message.reply('the chest will only work on yourself!');
		user.increment('gems', { by: 10 });
		message.reply('you open the treasure and find 10 gems laying within, await for you to collect them! Your pockets get heavier.');
		userItem.decrement('amount', { by: 1 });
	},
};
