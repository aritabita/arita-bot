const database = require('../../database/connect');
const Boost = database.import('../../models/boostPotion');
module.exports = {
	name: 'gb',
	async execute(message, userItem) {
		let target;
		if (!message.mentions.users.first()) target = message.author.id;
		else target = message.mentions.users.first().id;
		const user = await Boost.findOne({ where: { userId: target } });
		if (user) return message.reply('this potion cannot be used while you\'re experiencing the effects of another!');
		const rando = Math.floor(Math.random() * 4);
		let multiplier;
		let response;
		if (rando == 0) multiplier = 0.25, response = 'a very small';
		if (rando == 1) multiplier = 0.50, response = 'a strong';
		if (rando == 2) multiplier = 0.75, response = 'a great';
		if (rando == 3) multiplier = 1, response = 'an incredible';
		try {
			await Boost.create({
				userId: target,
				type: 'high',
				multiplier: multiplier,
				time: 50,
			});
		}
		catch(e) {
			return console.log(e);
		}
		message.reply(`drinking the prototype potion, you (or your target) feel ${response} reaction within your body. Gem gains have been increased temporarily!`);
		userItem.decrement('amount', { by: 1 });
	},
};
