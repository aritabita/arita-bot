const database = require('../../database/connect');
const Money = database.import('../../models/money');
module.exports = {
	name: 'lh',
	async execute(message, userItem) {
		const user = await Money.findOne({ where: { userId: message.author.id } });
		if (!user) return message.reply('error.');
		if (message.mentions.users.first()) return message.reply('this only works on yourself!');
		user.increment('gems', { by: 5 });
		message.reply('you return the lost horseshoe to a peculiar-looking horse. It pays you 5 gems in gratitude and hops away.');
		userItem.decrement('amount', { by: 1 });
	},
};
