const database = require('../../database/connect');
const Anniversary = database.import('../../models/anniversaryCurrency');
module.exports = {
	name: 'gc',
	async execute(message, userItem) {
		const user = await Anniversary.findOne({ where: { userId: message.author.id } });
		if (!user) return message.reply('error.');
		if (message.mentions.users.first()) return message.reply('the Clam will only share its secrets with its owner.');
		const random = Math.floor(Math.random() * 4);
		user.increment('bubbles', { by: random });
		message.reply(`the clam opens, sharing with you its bounty of ${random} little bubble(s)`);
		userItem.decrement('amount', { by: 1 });
	},
};
