const database = require('../../database/connect');
const Waluigi = database.import('../../models/waluigi');
const config = require('../../config.json');
module.exports = {
	name: 'in',
	async execute(message, userItem) {
		let target;
		if (!message.mentions.users.first()) target = message.author.id;
		else target = message.mentions.users.first().id;
		const user = await Waluigi.findOne({ where: { userId: target } });
		if (!user) {
			try {
				await Waluigi.create({
					userId: target,
					transformation: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
			userItem.decrement('amount', { by: 1 });
			return message.reply(config.waluigiTransformation[0]);
		}
		const transformationLevel = parseInt(user.transformation, 10);
		const nextLevel = transformationLevel + 1;
		const currentLevel = transformationLevel;
		if (nextLevel > config.waluigiTransformation.length) {
			return message.reply('user has fully become Waluigi! Wah!');
		}
		user.increment('transformation', { by: 1 });
		message.reply(config.waluigiTransformation[currentLevel]);
		userItem.decrement('amount', { by: 1 });
	},
};
