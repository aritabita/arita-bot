module.exports = {
	name: 'wah',
	async execute(message, userItem) {
		let mentionedUser;
		if (!message.mentions.users.first()) mentionedUser = message.guild.members.get(message.author.id);
		else mentionedUser = message.guild.members.get(message.mentions.users.first().id);
		mentionedUser.send('WAHH!').catch(() => message.reply('I couldn\'t reach the target! WAH!'));
		message.reply('//Report success here! Maybe!');
		userItem.decrement('amount', { by: 1 });
	},
};
