const database = require('../../database/connect');
const Boost = database.import('../../models/boostPotion');
module.exports = {
	name: 'ot',
	async execute(message, userItem) {
		let target;
		if (!message.mentions.users.first()) target = message.author.id;
		else target = message.mentions.users.first().id;
		const user = await Boost.findOne({ where: { userId: target } });
		if (user) return message.reply('the tank cannot be used while you\'re experiencing a gem boost!');
		try {
			await Boost.create({
				userId: target,
				type: 'rush',
				multiplier: 0.50,
				time: 30,
			});
		}
		catch(e) {
			return console.log(e);
		}
		message.reply('deeply inhaling the oxygen within the tank, you suddenly feel your body to be completely revitalized, and you generate more gems!');
		userItem.decrement('amount', { by: 1 });
	},
};
