module.exports = {
	name: 'cc',
	async execute(message) {
		return message.reply('this item cannot be used. If you have any, it\'ll trigger automatically. There\'s nothing more loving than the cuddle of a cuddlefish!');
	},
};
