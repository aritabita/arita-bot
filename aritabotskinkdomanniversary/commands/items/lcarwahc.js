const database = require('../../database/connect');
const Cards = database.import('../../models/cardInventory');

module.exports = {
	name: 'lcarwahc',
	async execute(message, userItem) {
		let user = await Cards.findOne({ where: { userId: message.author.id, cardKey: 'arwahc' } });
		if (!user) {
			try {
				await Cards.create({
					userId: message.author.id,
					cardKey: 'arwahc',
					amount: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Cards.findOne({ where: { userId: message.author.id, cardKey: 'arwahc' } });
		}
		user.increment('amount', { by: 1 });
		userItem.destroy();
		message.reply('you obtain the Wah Coin card');
	},
};
