const database = require('../../database/connect');
const PurpleGem = database.import('../../models/purpleGem');
module.exports = {
	name: 'nnc',
	async execute(message, userItem) {
		let user = await PurpleGem.findOne({ where: { userID: message.author.id } });
		if (!user) {
			try {
				await PurpleGem.create({
					userID: message.author.id,
					darkness: 0,
					purpleGem: 0,
				});
				user = await PurpleGem.findOne({ where: { userID: message.author.id } });
			}
			catch(e) {
				return console.log(e);
			}
		}
		if (!user) return message.reply('something went wrong!');
		const darkNumber = parseInt(user.darkness, 10);
		const total = darkNumber + 10;
		user.increment('darkness', { by: 10 });
		if (total > 99) {
			user.darkness = 0;
			await user.save();
			user.increment('purpleGem', { by: 1 });
			return message.reply('after consuming the dark candy, a Purple Gem emanates from your body.');
		}
		message.reply('upon consuming the candy, you feel darkness all around being absorbed into your body. At that moment, you come a bit at peace with your own dark side.');
		userItem.decrement('amount', { by: 1 });
	},
};
