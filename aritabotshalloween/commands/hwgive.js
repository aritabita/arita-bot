const config = require('../config.json');
const database = require('../database/connect');
const Candy = database.import('../models/candy');
module.exports = {
	name: 'hwgive',
	usage: '<item> <amount> <user>',
	description: 'Staff only. Allows for halloween item granting',
	forUser: false,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID) || message.member.roles.has(config.opID))
		{
			if (typeof args[0] == 'undefined') return message.reply('Must specify item!');
			if (args[0]) {
				if (isNaN(args[1])) return message.reply('I need to know how many!');
				const item = config.store.find(obj => obj.key == args[0]);
				if (!item) {
					if (args[0] !== 'candy') return message.reply('you must be mistaken, young one. Try again.');
					const user = await Candy.findOne({ where: { userId: message.mentions.users.first().id } });
					if (!user) return message.reply('Error. Tell Arita to look for line 21');
					user.increment('candy', { by: args[1] });
					return message.reply(`success! ${message.mentions.users.first()} has obtained ${args[1]}:candy:`);
				}
				const user = await Candy.findOne({ where: { userId: message.mentions.users.first().id } });
				if (!user) return message.reply('Error. Tell Arita to look for line 21');
				user.increment(`${item.db}`, { by: args[1] });
				message.reply(`success! ${message.mentions.users.first()} has obtained ${args[1]}${item.emoji}`);
			}
		}
	},
};
