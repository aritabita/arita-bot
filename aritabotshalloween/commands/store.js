const config = require('../config.json');
const database = require('../database/connect');
const Candy = database.import('../models/candy');
module.exports = {
	name: 'store',
	aliases: ['str', 'buy', 'by'],
	usage: '<item key>',
	description: 'Checks the store\'s contents, if used with an item key as second argument, it will provide a description of the item, while attempting to use candy to purchase the item',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return message.channel.send('It doesn\'t seem like this store takes online purchases. Really, this is just a really outdated business practice. You should leave a bad review on Yelp.');
		message.delete();
		if (typeof args[0] == 'undefined') {
			let response = 'Ehehehe! Welcome to my wandering shop, traveler! Take a look around, for the items available for sale are:\n';
			for (let i = 0; i < config.store.length; i++) {
				response += `${config.store[i].emoji}${config.store[i].name}[${config.store[i].key}]\n`;
			}
			response += 'Just let me know what you want! Use this command again with the [key] of the item that you\'d like to purchase.';
			message.reply(response);
			return;
		}
		if (args) {
			const item = config.store.find(obj => obj.key == args[0]);
			if (!item) return message.reply('huh? What are you talking about? I don\'t have such item in stock!');
			const sentMessage = await message.channel.send(`${item.emoji}${item.name}[${item.key}]\n${item.description}\nWould you like to purchase this item?`);
			await sentMessage.react('✅');
			await sentMessage.react('❌');
			const filter = (reaction, user) => {
				return user.id === message.author.id
				&& (reaction.emoji.name === '✅' || reaction.emoji.name === '❌');
			};
			try{
				const collected = await sentMessage.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] });
				if (collected.first().emoji.name === '❌')
				{
					sentMessage.delete();
					return message.channel.send('Window shopping, eh? Fine. Let me know if you want anything else.');
				}

			}
			catch (error) {
				sentMessage.delete();
				return message.channel.send('Hey! I don\'t have your kind of time! Hurry up and decide next time!');
			}
			const user = await Candy.findOne({ where: { userId: message.author.id } });
			if (!user) {
				try {
					await Candy.create({
						userId: message.author.id,
						candy: 0,
						exp: 0,
						level: 1,
						foxMask: 0,
						ghostCloth: 0,
						lostFigurine: 0,
						abandonedTie: 0,
						wyvernTail: 0,
						sparkler: 0,
						shapeshifterHair: 0,
						unidentifiableGenus: 0,
						dinosaurFang: 0,
						pandaMask: 0,
						pranksterSoul: 0,
						empressCrown: 0,
					});
				}
				catch(e) {
					return console.log(e);
				}
				sentMessage.delete();
				message.reply('you don\'t have enough :candy: to pay for this item!');
			}
			sentMessage.delete();
			if (user.candy < item.price) return message.reply('stop wasting my time! You don\'t have enough :candy: to pay for this item!');
			user.increment(`${item.db}`, { by: 1 });
			user.decrement('candy', { by: item.price });
			message.reply('hehehe! There you go! Pleasure doing business with you, and come back soon, I\'ll be in town until next month!');
		}
	},
};
