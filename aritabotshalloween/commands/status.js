const config = require('../config.json');
const database = require('../database/connect');
const Candy = database.import('../models/candy');
module.exports = {
	name: 'status',
	aliases: ['st', 'inventory', 'in', 'level', 'lvl'],
	usage: '<n>',
	description: 'Allows the user to check their current Spooky Experience, alongside their inventory of items from the Itinerant Store. Adding an n after performing the command will display the experience points in a numerical value instead of graphical',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return message.channel.send('It doesn\'t seem like you can check what you have when you\'re away from your bag!');
		message.delete();
		let user = await Candy.findOne({ where: { userId: message.author.id } });
		if (!user) {
			try {
				await Candy.create({
					userId: message.author.id,
					candy: 0,
					exp: 0,
					level: 1,
					foxMask: 0,
					ghostCloth: 0,
					lostFigurine: 0,
					abandonedTie: 0,
					wyvernTail: 0,
					sparkler: 0,
					shapeshifterHair: 0,
					unidentifiableGenus: 0,
					dinosaurFang: 0,
					pandaMask: 0,
					pranksterSoul: 0,
					empressCrown: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Candy.findOne({ where: { userId: message.author.id } });
		}
		if (!user) return message.reply('an unexpected error has occurred. Please let Arita know.');
		let response;
		const endlevel = user.level + 1;
		if (typeof args[0] == 'undefined') {
			response = `Current experience:\n${user.level} `;
			const total = Math.trunc(user.exp / 50);
			for(let i = 0; i < total; i++)
			{
				response += '💀';
			}
			for (let i = total; i < endlevel; i++)
			{
				response += '🔴';
			}
			response += ` ${endlevel}\nPotential Rewards: ${endlevel * 2}🍬\n`;
		}
		else if (args != 'n') {return message.reply('that\'s not a valid argument!');}
		else {
			response = `Current experience:\n**${user.level}** ${user.exp.toFixed(2)}/${endlevel * 50} **${endlevel}**\nPotential Rewards: ${endlevel}🍬\n`;
		}
		for(let i = 0; i < config.store.length; i++) {
			response += `${config.store[i].emoji} ${user[config.store[i].db]}  `;
		}
		response += `:candy:${user.candy}`;
		message.reply(response);
	},
};
