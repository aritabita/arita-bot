const config = require('../config.json');
const database = require('../database/connect');
const Candy = database.import('../models/candy');
module.exports = {
	name: 'use',
	aliases: ['us'],
	usage: '<key> <user>',
	description: 'Uses purchased item on self or another member.',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return message.channel.send('Unfortunately, it doesn\'t seem like you have access to your items if no one is around.');
		message.delete();
		if (typeof args[0] == 'undefined') return message.reply('you must specify which item you want to use!');
		const user = await Candy.findOne({ where: { userId: message.author.id } });
		if (!user) return message.reply('it doesn\'t seem like you have awakened your inner spooky power yet...');
		const item = config.store.find(obj => obj.key == args[0]);
		if (!item) return message.reply('it doesn\'t seem like an item like that exists...');
		if (args[0] == 'ps') {
			if (user[item.db] < 1) return message.reply('you don\'t have any of that item!');
			const number1 = Math.floor(Math.random() * config.adjectives.length);
			const number2 = Math.floor(Math.random() * config.nouns.length);
			const adjective = config.adjectives[number1];
			const noun = config.nouns[number2];
			let reply;
			if (!message.mentions.users.first()) {
				const mentionedUser = message.guild.members.get(message.author.id);
				mentionedUser.setNickname(`${adjective} ${noun}`);
				reply = 'taking a deep sniff at the liquid within the Prankster\'s Soul, you lose all sense of self! What a horrible night to have a curse.';
				mentionedUser.roles.add('505905035889082393');
			}
			else {
				const mentionedUser = message.guild.members.get(message.mentions.users.first().id);
				if (mentionedUser.roles.has(config.adminID)) return message.reply('the target was unaffected by your attempts!');
				mentionedUser.setNickname(`${adjective} ${noun}`);
				reply = 'throwing the flask of liquid at your target, they are forced to smell the terrible liquid, causing them to lose all sense of self! What a horrible night to have a curse.';
				mentionedUser.roles.add('505905035889082393');
			}
			user.decrement(`${item.db}`, { by: 1 });
			message.reply(reply);
		}
		else {
			if (user[item.db] < 1) return message.reply('you don\'t have any of that item!');
			const rolesToRemove = [];
			let reply;
			let mentionedUser;
			for (let i = 0; i < config.store.length; i++) {
				if (!config.store[i].id) continue;
				rolesToRemove.push(config.store[i].id);
			}
			if (!message.mentions.users.first()) {
				mentionedUser = message.guild.members.get(message.author.id);
				reply = 'you apply the outfit and transform into a new self. Boo!';
			}
			else {
				mentionedUser = message.guild.members.get(message.mentions.users.first().id);
				if (mentionedUser.roles.has(config.adminID)) return message.reply('the target was unaffected by your attempts!');
				reply = 'you sucessfully trick your target and apply a new costume on them, changing them to a new self';
			}
			for (let i = 0; i < rolesToRemove.length; i++) {
				if (!mentionedUser.roles.has(rolesToRemove[i])) continue;
				mentionedUser.roles.remove(rolesToRemove[i]);
			}
			mentionedUser.roles.add(item.id);
			user.decrement(`${item.db}`, { by: 1 });
			message.reply(reply);
		}
	},
};
