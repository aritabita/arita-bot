const database = require('../database/connect');
const Money = database.import('../models/money');
const config = require('../../config');
module.exports = {
	name: 'dg',
	async execute(message, userItem) {
		if (message.member.roles.has(config.role0)) return message.reply('innocents are unable to use this item.');
		const userGems = await Money.findOne({ where: { userId: message.author.id } });
		if (!userGems) return message.reply('this item cannot be used for some reason.');
		userGems.increment('gems', { by: 10 });
		userItem.decrement('amount', { by: 1 });
		message.reply('the Dark Gem shines, and dissapears. At that point, you feel your pockets to be heavier.');
	},
};
