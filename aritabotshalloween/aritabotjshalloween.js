const Candy = database.import('./models/candy');

else if (message.channel.parentID == '498295888955965470')
{
	const count = message.content.split(' ').length;
	const userName = await Money.findOne({ where: { userId: message.author.id } });
	const moneyTotal = count * config.rp_word_gain;
	if (!userName) {
		try {
			await Money.create({
				userId: message.author.id,
				gems: 1,
			});
		}
		catch(e) {
			return console.log(e);
		}
	}
	else {
		userName.increment('gems', { by: moneyTotal });
	}
	tracker(message, moneyTotal);
	halloween(message, moneyTotal);
	candyDropper(message);
}

async function halloween(message, args) {
	const user = await Candy.findOne({ where: { userId: message.author.id } });
	if (!user)
	{
		try {
			await Candy.create({
				userId: message.author.id,
				candy: 0,
				exp: args,
				level: 1,
				foxMask: 0,
				ghostCloth: 0,
				lostFigurine: 0,
				abandonedTie: 0,
				wyvernTail: 0,
				sparkler: 0,
				shapeshifterHair: 0,
				unidentifiableGenus: 0,
				dinosaurFang: 0,
				pandaMask: 0,
				pranksterSoul: 0,
				empressCrown: 0,
			});
		}
		catch(e) {
			return console.log(e);
		}
		return;
	}
	if (user.level == 20) return;
	const nextLevel = user.level + 1;
	const earns = args * 3;
	const nextExp = nextLevel * 50;
	const reward = nextLevel * 2;
	const currentExp = user.exp + args;
	await user.increment('exp', { by : earns });
	if (currentExp >= nextExp)
	{
		message.channel.send(`Boo! You have leveled up to level ${nextLevel}!`);
		await user.increment('candy', { by: reward });
		await user.increment('level', { by: 1 });
		user.exp = 0;
		await user.save();
		// 20 reward goes here
	}
}

async function candyDropper(message) {
	const randomNumber = Math.floor(Math.random() * 101);
	if (randomNumber == 1)
	{
		let user = await Candy.findOne({ where: { userId: message.author.id } });
		if (!user)
		{
			try {
				await Candy.create({
					userId: message.author.id,
					candy: 0,
					exp: 0,
					level: 1,
					foxMask: 0,
					ghostCloth: 0,
					lostFigurine: 0,
					abandonedTie: 0,
					wyvernTail: 0,
					sparkler: 0,
					shapeshifterHair: 0,
					unidentifiableGenus: 0,
					dinosaurFang: 0,
					pandaMask: 0,
					pranksterSoul: 0,
					empressCrown: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Candy.findOne({ where: { userId: message.author.id } });
		}
		user.increment('candy', { by: 1 });
		message.author.send('Boo! You found 1 🍬! Happy spoops!').catch(function() {
			return;
		});
	}
}
