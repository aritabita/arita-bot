##Presenting Arita Bot, The Kinkdom's own gem system and overall fun-time bot

Arita bot contains different tools to make the user experience at the server a smoother one:

- Tailored specifically for Discord.
- Unique progression system tied to each member's ID.
- The ability for users to get rewards depending on their progression with the in-server currency of Gems.
- Built-in trading functionalities, allowing members to help each other grow, with trading being unlocked after a certain amount of progression.
- Support for different event functionalities such as April Fools or Anniversary events.
- The ability to rent rooms using Gems
- Weekly random challenges that reward Gems upon completion, with gems scaling depending on the amount of progression the user has made.
- Unity game interactions through sockets. (CURRENTLY ON HOLD)

---

Thank you for checking out Arita bot! Have a nice day
