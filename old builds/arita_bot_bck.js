const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json');
const fs = require('fs');
const sql = require('sqlite');
const cron = require('node-cron');
const channelNameRegex = /^[\w-]+$/
const Sequelize = require('sequelize');
sql.open('./money.sqlite');
// Hello


//Server stuff
// var app = require('http').createServer(handler);
// var io = require('socket.io').listen(app);
//
//
// var mySocket = 0;
//
// app.listen(3000);
//
// function handler (req, res) {
//     fs.readFile(__dirname + '/index.html',
//     function (err, data) {
//         if (err) {
//           res.writeHead(500);
//           return res.end('Error loading index.html');
//         }
//         res.writeHead(200);
//         res.end(data);
//   });
// }
//
// io.sockets.on('connection', function (socket) {
//   console.log('Page connected!');
//   mySocket = socket;
// });
//
// var dgram = require('dgram');
// var server = dgram.createSocket('udp4');
//
// server.on('message', function (msg, rinfo) {
//     var completeMessage = msg.toString().split(" ");
//     var channeltosend = client.channels.get("387459783554367488");
//     channeltosend.send(completeMessage[0] + " got a score of " + completeMessage[1]);
//
// })
//
// server.on('listening', function(){
//   var address = server.address();
//   console.log("The UDP server is currently tuning in to " + address.address + ":" + address.port);
// });
//
// server.bind(41181);
//
//
// function highscore(name, score)
// {
//   sql.get(`SELECT * FROM highscore`)
//   sql.run('INSERT INTO highscore (name, score) VALUES (?, ?)', name, score);
// }


//Bot
var blacklist = [ "ooc",  "character", "test", "search", "chat", "info", "information", "contest"];
// Variables to check the message
        const w = "WWW";
        const com = "COM";
        const http = "HTTP";
        const https = "HTTPS";
      const sequelize = new Sequelize({
            dialect: 'sqlite',
            logging: false,
            operatorsAliases: false,
            // SQLite only
            storage: 'money.sqlite',
        });
      const Money = sequelize.define('money', {
        userId: {
          type: Sequelize.STRING,
          primaryKey: true,
  },
        gems: Sequelize.FLOAT,
      },
    {
      timestamps: false,
    });
      const Rent = sequelize.define('rent', {
          ownerID: Sequelize.STRING,
          roomID: {
            type: Sequelize.STRING,
            unique: true,
          },
          days: Sequelize.INTEGER,
          topic: Sequelize.STRING,
      });
const Pranks = sequelize.define('pranks', {
		 ownerID: {
			 type: Sequelize.STRING,
			 unique: true,
		 },
		 bomb: Sequelize.INTEGER,
	  });
const Team = sequelize.define('team', {
	name:Sequelize.STRING,
	amount: Sequelize.INTEGER,
});

      cron.schedule('0 0 0 * * * *', async function(){
        const results = await Rent.findAll();
        results.forEach(async result => {
          if (result.days < 0) return;
          result.days = result.days - 1;
          await result.save();
          var searchedChannel = client.channels.get(result.roomID);
          if (!searchedChannel) return;
          if (!result.topic)
          {
            searchedChannel.setTopic(`This room has ${result.days} days left.`)
          }
          else {
          searchedChannel.setTopic(`${result.topic} || This room has ${result.days} days left.`)
          }
          if (result.days === 0)
          {
                var logchannel = client.channels.get(config.rent_log);
                logchannel.send(`❌ <@${result.ownerID}>'s room ${searchedChannel.name} was deleted.`)
                searchedChannel.delete();
          }
          else if (result.days === 1)
          {
                searchedChannel.send(`Dawn of the Final Day, <@${result.ownerID}>! Type /rraddtime ###(number of days) to add more days!`);

          }
        });
      });
client.once('ready', () => {
	Rent.sync();
	Pranks.sync();
	Team.sync();
});
client.login(config.token);
client.on('ready', () => {console.log(`Arita has been booted up! Arita operates in ${client.guilds.size} servers, and for ${client.users.size} people!`);
});
client.on('error', (e) => console.error(e));
client.on('warn', (e) => console.warn(e));
client.on('debug', (e) => console.info(e));
client.on('ready', () => {
//client.user.setGame('Loli Empress Simulator')
})
client.on('messageDelete', async(messageDelete) => {
	if (config.rp_subchannel.includes(messageDelete.channel.parentID) && (!messageDelete.content.startsWith('(')))
	{
		const rem = messageDelete.content.split(' ').length * 0.02;
		const user = await Money.findOne({ where: { userId: messageDelete.author.id } });
		user.decrement('gems', { by: rem });
	}
});
client.on('message', async(message) =>{
  if (message.channel.type !== 'text') return;
  if (message.author.bot) return;
  //split the message up based on a space and put into an array
  const msgArray = message.content.split(" ");
  const args = message.content.split(' ').slice(1);

  if (message.content.startsWith(config.prefix + 'eval')) {
      if(message.author.id !== config.ownerID) return;
      try {
        message.delete();
        var code = args.join(' ');
        var evaled = eval(code);

        if (typeof evaled !== 'string')
          evaled = require('util').inspect(evaled);

        message.channel.send(clean(evaled), {code:'xl'});
      } catch (err) {
        message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
      }
    }
	// else if(message.channel.id == '316438475098554368')
	// {
	// 	const i = await Math.floor((Math.random() * 5) + 1)
	// 	if (i == 5)
	// 	{
	// 		const user = await Pranks.findOne({ where: { ownerID: message.author.id } });
	// 		if (!user)
	// 		{
	// 			try{
	// 				await Pranks.create({
	// 					ownerID: message.author.id,
	// 					bomb: 1,
	// 				});
	// 				return message.author.send('Psssst! You\'ve found a 💣 **Prankster\'s Balloon!** It sure would be baaad if you /throw @someone !');
	// 			}
	// 			catch (e) {
	// 				return message.author.send('Something has gone wrong, please tell Arita');
	// 			}
	// 		}
	// 		else
	// 		{
	// 			user.increment('bomb', { by: 1 });
	// 			message.author.send('Psssst! You\'ve found a 💣 **Prankster\'s Balloon!** It sure would be baaad if you /throw @someone !');
	// 		}
	// 	}
	// }
  else if (message.channel.id == config.main_chat || message.channel.id == config.lewd_chat || message.channel.id == config.conversation_chat)
      {
        sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
          if (!row)
          {
             sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
          }
          else
          {
            sql.run(`UPDATE money SET gems = ${row.gems + config.general_gain} WHERE userId = ${message.author.id}`);
          }

        }).catch(() => {
          console.error();
          sql.run(`CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)`).then(() => {
            sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
          });
        });
        if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) return;

          for(let i = 0; i < msgArray.length; i++) {                                      // loop through the array
                     var index = msgArray[i].toUpperCase();                                  // convert the strings to uppercase
                     if(index.indexOf(w+".") !== -1  && index.indexOf("."+com) !== -1) {     // checks to see if the word contains www. and .com

                         message.delete();
                         message.author.send(`Arita's words echo through your mind..."Hey! ${message.author}! No links allowed in main!"`)
                     }
                     else if(index.indexOf(http) !== -1 || index.indexOf(https) !== -1){          // checks to see if word contains http or https
                         if(index.indexOf(w+".") || index.indexOf("."+com)){                 // checks to see if it contains www. or .com
                             message.delete();
                             message.author.send(`Arita's words echo through your mind..."Hey! ${message.author}! No links allowed in main!"`)
                         }
                     }
                     //console.log(msgArray[i] + "\n");
                 }

    }

    else if (message.content.startsWith(config.prefix + `reset`))
      {
        if (message.member.roles.has(config.adminID)) //|| message.member.roles.has(config.modID))
        {
          message.delete();
          var rand = config.resetMessages[Math.floor(Math.random() * config.resetMessages.length)];
          message.channel.send({embed: {
          color: 3447003,
          description: rand
        }});
          return;
        }
        message.delete();
        if (message.channel.parentID != config.rent_cat) return message.reply("Not a Rented Room.");
        const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
        if (!owner) return;
        if (owner.ownerID != message.author.id) return message.reply("Not the channel owner.");
        message.channel.send({embed: {
        color: 3447003,
        description: "This channel has been reset as per the Owner's request! \n The first post after this gets to send the foundation, unless the Owner wishes it otherwise. Anyone who wishes to join in should read through the ongoning roleplay and ask the Owner before doing so."
      }});
      }
else if (message.channel.parentID == config.main_category)
{
    sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
      if (!row)
      {
         sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
      }
      else
      {
        sql.run(`UPDATE money SET gems = ${row.gems + config.general_gain} WHERE userId = ${message.author.id}`);
      }

    }).catch(() => {
      console.error();
      sql.run(`CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)`).then(() => {
        sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
      });
    });
}


else if (message.content.startsWith(config.prefix + `lock`))
{
  message.delete();
  if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID))
  {
    message.guild.channels.find("name", "main_chat").overwritePermissions('296324926745608199',
    {
      SEND_MESSAGES: false
    })
    message.guild.channels.find("name", "staff_lounge").send("@here SERVER'S UNDER HEAVY RAID, ALL UNCHARTED USERS HAVE BEEN MUTED. FIRE THE WOOSH BLASTER. SPARE NO ONE!");
  }
}

else if (message.content.startsWith(config.prefix + `lift`))
{
  message.delete();
  if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID))
  {
    message.guild.channels.find("name", "main_chat").overwritePermissions('296324926745608199',
    {
      SEND_MESSAGES: true
    })
    message.guild.channels.find("name", "staff_lounge").send("@here Raid lockdown lifted. Tals ban count still is as high as Mount Everest. Be wary of introductions for the next hour");
  }
}

// else if (gg.includes(message.channel.id))
//    {
//      sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
//        if (!row)
//        {
//           sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
//        }
//        else
//        {
//          sql.run(`UPDATE money SET gems = ${row.gems + config.gg_gain} WHERE userId = ${message.author.id}`);
//        }
//
//      }).catch(() => {
//        console.error();
//        sql.run(`CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)`).then(() => {
//          sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
//        });
//      });
//    }
   else if (config.image_subchannel.includes(message.channel.parentID) && (message.attachments.size)  || (config.image_subchannel.includes(message.channel.parentID)) && (message.content.startsWith('https://'))) {
  sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
    if (!row)
    {
       sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
    }
    else
    {
      sql.run(`UPDATE money SET gems = ${row.gems + config.image_gain} WHERE userId = ${message.author.id}`);
    }

  }).catch(() => {
    console.error();
    sql.run(`CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)`).then(() => {
      sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
    });
  });
 }

 else if (message.content.startsWith(config.prefix + `rraddtime`))
 {
   message.delete();
   if (message.channel.parentID != config.rent_cat) return message.reply("Not a Rented Room.");
   const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
   if (!owner) return;
   if (owner.ownerID != message.author.id) return message.reply("Not the channel owner.");
   var rentedDays = Math.round(args[0]);
   if (isNaN(rentedDays)) return message.reply("Invalid number of days.");
   const user = await Money.findOne({ where: { userId: message.author.id } });
   var price = rentedDays * config.rent_cost;
   if (user) {
     if (user.gems < price) return message.reply("Not enough gems!");
     const sentMessage = await message.channel.send(`This will cost ${price} :gem:. Are you sure?`)
       await sentMessage.react('✅');
       await sentMessage.react('❌')
       const filter = (reaction, user) => {
           return user.id === message.author.id
               && (reaction.emoji.name === '✅' || reaction.emoji.name === '❌');
       };
         try{
               const collected = await sentMessage.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] });
                 if (collected.first().emoji.name === '❌')
                 {
                   sentMessage.delete();
                   return message.channel.send('Transaction cancelled');
                 }

               }
               catch (error) {
                 sentMessage.delete();
                 return message.channel.send('User took too long to reply, transaction canceled.');
               }
     user.decrement('gems', { by: price });
     sentMessage.delete();
   }
   owner.days = owner.days + parseInt(args[0]);
   await owner.save();
   message.reply(`Days increased by ${rentedDays}!`);
   if (!owner.topic)
   {
     message.channel.setTopic(`This room has ${owner.days} days left.`)
   }
   else {
   message.channel.setTopic(`${owner.topic} || This room has ${owner.days} days left.`)
   }
   var logchannel = client.channels.get(config.rent_log);
   logchannel.send(`💎 ${message.author.username} renewed ${message.channel.name} for ${rentedDays} days.`)
 }

 else if (message.content.startsWith(config.prefix + `rraddmember`))
 {
   message.delete();
   if (message.channel.parentID != config.rent_cat) return message.reply("Not a Rented Room.");
   const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
   if (!owner) return;
   if (owner.ownerID != message.author.id) return message.reply("Not the channel owner.");
   if (!message.mentions.members.first()) return message.reply("Must specify a user.")
   await message.channel.overwritePermissions(message.mentions.members.first(), {
     VIEW_CHANNEL: true,
     SEND_MESSAGES: true,
   });
   message.reply(`Welcome ${message.mentions.members.first()}`)
 }
 else if (message.content.startsWith(config.prefix + `rrname`))
 {
   message.delete();
   if (message.channel.parentID != config.rent_cat) return message.reply("Not a Rented Room.");
   const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
   if (!owner) return;
   if (owner.ownerID != message.author.id) return message.reply("Not the channel owner.");
   var currentName = message.channel.name;
   if (!args[0]) return message.reply("Must contain a name");
   if (!channelNameRegex.test(args[0]))
     return message.reply('Only alphanumeric characters allowed!');
   if (args[0].length > 30)
     return message.reply('Channel names may be up to 30 characters long!');
   await message.channel.setName(args[0]);
   await message.reply("Name set.");
   var logchannel = client.channels.get(config.rent_log);
   logchannel.send(`📝 ${message.author.username} changed ${currentName}'s name to ${message.channel.name}.`)
 }
 else if (message.content.startsWith(config.prefix + `rrsetchannel`))
 {
   message.delete();
   if (message.channel.parentID != config.rent_cat) return message.reply("Not a Rented Room.");
   const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
   if (!owner) return;
   if (owner.ownerID != message.author.id) return message.reply("Not the channel owner.");
   if (!args[0]) return message.reply("Must contain a name");
   if (args[0].toLowerCase() == "private")
   {
     message.channel.overwritePermissions(message.guild.defaultRole, {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.reply("Channel set.");
   }
   else if (args[0].toLowerCase() == "public")
   {
     message.channel.overwritePermissions(message.guild.defaultRole, {
       VIEW_CHANNEL: null,
       SEND_MESSAGES: true,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: true,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: true,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: true,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: true,
     });
    return message.reply("Channel set.");
   }
   else if (args[0].toLowerCase() == "readonly")
   {
     message.channel.overwritePermissions(message.guild.defaultRole, {
        VIEW_CHANNEL: null,
        SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: false,
     });
    return message.reply("Channel set.");
  }
   else if (args[0].toLowerCase() == "dark")
   {
     message.channel.overwritePermissions(message.guild.defaultRole, {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: true,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     return message.reply("Channel set.");
   }
   else if (args[0].toLowerCase() == "nonhuman")
   {
     message.channel.overwritePermissions(message.guild.defaultRole, {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: true,
     });
     return message.reply("Channel set.");
   }
   else if (args[0].toLowerCase() == "kink")
   {
     message.channel.overwritePermissions(message.guild.defaultRole, {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: true,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     return message.reply("Channel set.");
   }
   else if (args[0].toLowerCase() == "fandom")
   {
     message.channel.overwritePermissions(message.guild.defaultRole, {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.fandom), {
       VIEW_CHANNEL: true,
       SEND_MESSAGES: true,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.kink), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.dark), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     message.channel.overwritePermissions(message.guild.roles.get(config.nonhuman), {
       VIEW_CHANNEL: false,
       SEND_MESSAGES: false,
     });
     return message.reply("Channel set.");
   }
   else return message.reply("Channel can only be set to private or public.");
 }
 else if (message.content.startsWith(config.prefix + `rrsettopic`))
 {
   message.delete();
   if (message.channel.parentID != config.rent_cat) return message.reply("Not a Rented Room.");
   const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
   if (!owner) return;
   if (owner.ownerID != message.author.id) return message.reply("Not the channel owner.");
   if (!args) return message.reply("Must contain a topic");
   var topic = args.join(' ');
   if (topic.length > 500) return message.reply("Topic is too long!");
   await message.channel.setTopic(`${topic} || This channel has ${owner.days} days left.`);
   owner.topic = topic;
   await owner.save();
   message.reply("Topic set!");
   var logchannel = client.channels.get(config.rent_log);
   logchannel.send(`✏ ${message.author.username} changed ${message.channel.name}'s topic to ${message.channel.topic}`)
 }

 else if (config.rp_subchannel.includes(message.channel.parentID) && (!message.content.startsWith('(')))
 {
    var loopOver = false;
    for(i = 0; i < blacklist.length; i++) {
        if(message.channel.name.includes(blacklist[i]))
        {
          loopOver = true;
          return;
        }
      }
      if (loopOver) return;
      var count = message.content.split(' ').length;
     sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
       if (!row)
       {
          sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
       }
       else
       {
         sql.run(`UPDATE money SET gems = ${row.gems + config.rp_gain + (count * config.rp_word_gain)} WHERE userId = ${message.author.id}`);
       }

     }).catch(() => {
       console.error();
       sql.run(`CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)`).then(() => {
         sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
       });
     });
		// const member = message.member;
		// const roleNameArray = ['Team Revvy', 'Team Pandora', 'Team Chibi', 'Team Rin', 'Team Tal'];
		// const checkVar = member.roles.some(roles=>roleNameArray.includes(roles.name));
		// if (!checkVar) return;
		// const teamp = count * config.rp_word_gain;
		// if (member.roles.has(config.teams[0]))
		// {
		// 	const team = await Team.findOne({ where: { name: 'rev' } });
		// 	team.increment('amount', { by: teamp });
		// }
		// else if (member.roles.has(config.teams[1]))
		// {
		// 	const team = await Team.findOne({ where: { name: 'pan' } });
		// 	team.increment('amount', { by: teamp });
		// }
		// else if (member.roles.has(config.teams[2]))
		// {
		// 	const team = await Team.findOne({ where: { name: 'chi' } });
		// 	team.increment('amount', { by: teamp });
		// }
		// else if (member.roles.has(config.teams[3]))
		// {
		// 	const team = await Team.findOne({ where: { name: 'rin' } });
		// 	team.increment('amount', { by: teamp });
		// }
		// else if (member.roles.has(config.teams[4]))
		// {
		// 	const team = await Team.findOne({ where: { name: 'tal' } });
		// 	team.increment('amount', { by: teamp });
		// }
		// const world = await Team.findOne({ where: { name: 'world' } });
		// world.increment('amount', { by: teamp });
 }
 else if (config.event_subchannel.includes(message.channel.parentID) && (!message.content.startsWith('(')))
 {
   var loopOver = false;
   for(i = 0; i < blacklist.length; i++) {
       if(message.channel.name.includes(blacklist[i]))
       {
         loopOver = true;
         return;
       }
     }
     if (loopOver) return;
     if (message.channel.name.includes("open"))
              {
               sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
                 if (!row)
                 {
                   sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
                 }
                 else
                 {
                   sql.run(`UPDATE money SET gems = ${row.gems + config.event_casual_gain} WHERE userId = ${message.author.id}`);
                 }

               }).catch(() => {
                 console.error();
                 sql.run(`CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)`).then(() => {
                   sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
                 });
               });
             }
         else
            {
               var count = message.content.split(' ').length;
              sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
                if (!row)
                {
                   sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
                }
                else
                {
                  sql.run(`UPDATE money SET gems = ${row.gems + config.rp_gain + (count * config.event_word_gain)} WHERE userId = ${message.author.id}`);
                }

              }).catch(() => {
                console.error();
                sql.run(`CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)`).then(() => {
                  sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
                });
              });

          }

      }
 else if (config.casual_subchannel.includes(message.channel.parentID))
 {
   var loopOver = false;
   for(i = 0; i < blacklist.length; i++) {
       if(message.channel.name.includes(blacklist[i]))
       {
         loopOver = true;
         return;
       }
     }
     if (loopOver) return;
             sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
               if (!row)
               {
                  sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
               }
               else
               {
                 sql.run(`UPDATE money SET gems = ${row.gems + config.varied_gain} WHERE userId = ${message.author.id}`);
               }

             }).catch(() => {
               console.error();
               sql.run(`CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)`).then(() => {
                 sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
               });
             });
			//  const member = message.member;
	 		// const roleNameArray = ['Team Revvy', 'Team Pandora', 'Team Chibi', 'Team Rin', 'Team Tal'];
	 		// const checkVar = member.roles.some(roles=>roleNameArray.includes(roles.name));
	 		// if (!checkVar) return;
	 		// const teamp = config.rp_varied_gain;
	 		// if (member.roles.has(config.teams[0]))
	 		// {
	 		// 	const team = await Team.findOne({ where: { name: 'rev' } });
	 		// 	team.increment('amount', { by: teamp });
	 		// }
	 		// else if (member.roles.has(config.teams[1]))
	 		// {
	 		// 	const team = await Team.findOne({ where: { name: 'pan' } });
	 		// 	team.increment('amount', { by: teamp });
	 		// }
	 		// else if (member.roles.has(config.teams[2]))
	 		// {
	 		// 	const team = await Team.findOne({ where: { name: 'chi' } });
	 		// 	team.increment('amount', { by: teamp });
	 		// }
	 		// else if (member.roles.has(config.teams[3]))
	 		// {
	 		// 	const team = await Team.findOne({ where: { name: 'rin' } });
	 		// 	team.increment('amount', { by: teamp });
	 		// }
	 		// else if (member.roles.has(config.teams[4]))
	 		// {
	 		// 	const team = await Team.findOne({ where: { name: 'tal' } });
	 		// 	team.increment('amount', { by: teamp });
	 		// }
			// const world = await Team.findOne({ where: { name: 'world' } });
			// world.increment('amount', { by: teamp });
  }

 else if (message.channel.parentID == config.staff)
 {
  sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
    if (!row)
    {
       sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
    }
    else
    {
      sql.run(`UPDATE money SET gems = ${row.gems + config.staff_gain} WHERE userId = ${message.author.id}`);
    }

  }).catch(() => {
    console.error();
    sql.run(`CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)`).then(() => {
      sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
    });
  });
 }

 else if (!message.content.startsWith(config.prefix)) return;
    else if (message.content.startsWith(config.prefix + 'money'))
    {
      message.delete();
      message.reply(" sorry! /money has been changed into /gems")
    }
    else if (message.content.startsWith(config.prefix + 'gems'))
    {
      message.delete();
      sql.get(`SELECT * FROM money WHERE userId = '${message.author.id}'`).then(row => {
        if (!row) return message.reply(`you have no`+ config.currency);
        message.reply(`you have ${row.gems.toFixed(2)}` + config.currency);
      });
    }
    else if (message.content.startsWith(config.prefix + 'arita'))
     {
       message.delete();
       message.channel.send('Arita shall rule forever');
     } else
     //prefix changing
     if (message.content.startsWith(config.prefix + 'prefix'))
     {
       //Gets prefix from command! (After 'Prefix')
      let newPrefix = message.content.split(' ').slice(1, 2)[0];
       //changes the file!
       config.prefix = newPrefix;
       fs.writeFile('./config.json', JSON.stringify(config));
     }
else if (message.content.startsWith(config.prefix + `deldel`))
{
  if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID))
  {
    message.channel.bulkDelete(50);
  }
}

else if (message.content.startsWith(config.prefix + `size`))
{
  message.reply(message.guild.channels.size);
}

// else if (message.content.startsWith(config.prefix + 'teamstatus'))
// {
// 		message.delete();
// 		const rev = await Team.findOne({ where: { name: 'rev' } });
// 		const pan = await Team.findOne({ where: { name: 'pan' } });
// 		const chi = await Team.findOne({ where: { name: 'chi' } });
// 		const rin = await Team.findOne({ where: { name: 'rin' } });
// 		const tal = await Team.findOne({ where: { name: 'tal' } });
// 		const world = await Team.findOne({ where: { name: 'world' } });
// 		return message.reply(`Team Rev has ${rev.amount.toFixed(2)}<:Revvy:339357194946347010>, Team Pan has ${pan.amount.toFixed(2)}<:Pandora:330294261079605248>, Team Chibi has ${chi.amount.toFixed(2)}<:chibi:425113335759896597>, Team Rin has ${rin.amount.toFixed(2)}<:Rin:402957365483143170>, and Team Tal has ${tal.amount.toFixed(2)}<:Giant:320415162542587904>, for a total of ${world.amount.toFixed(2)}💎`);
// }
// else if (message.content.startsWith(config.prefix + 'happybirthday'))
// {
// 		message.delete();
// 		const member = message.member
// 		const roleNameArray = ['Team Revvy', 'Team Pandora', 'Team Chibi', 'Team Rin', 'Team Tal'];
// 		const checkVar = member.roles.some(roles=>roleNameArray.includes(roles.name));
// 		if (checkVar) return;
// 		let userID = message.author.id;
// 		const userName = await Money.findOne({ where: { userId: message.author.id } });
// 		if (userName) {
// 			userName.increment('gems', { by: 10 });
// 			userID = userID.slice(-1);
// 			if (userID == 0 || userID == 1)
// 			{
// 				message.member.addRole(config.teams[0]);
// 				return message.reply('Arita bot thanks you for your wishes, giving you a gift of her own, some gems. Playfully, she puts a flag on your head with Revvy\'s face on it');
// 			}
// 			else if (userID == 2 || userID == 3)
// 			{
// 				message.member.addRole(config.teams[1]);
// 				return message.reply('after congratulating Arita bot, you weirdly feel your pockets heavier. You also find a tatoo of Pandora on your left arm');
// 			}
// 			else if (userID == 4 || userID == 5)
// 			{
// 				message.member.addRole(config.teams[2]);
// 				return message.reply('Arita bot pats your back and hands you a shirt with Chibi\'s face on it, alongside a small sack of gems. Says you have to wear that shirt and support the team');
// 			}
// 			else if (userID == 6 || userID == 7)
// 			{
// 				message.member.addRole(config.teams[3]);
// 				return message.reply('Arita bot gives you a big hug and slips some gems into your pocket during it. After the hug though, you have a very particular smell, like fox.');
// 			}
// 			else{
// 				message.member.addRole(config.teams[4]);
// 				return message.reply('right after congratulating Arita bot, you feel somewhat...different. You get a terrible urge to crash some parties and ban some fools. You got extra gems, too');
// 			}
// 		}
//
// }

// else if (message.content.startsWith(config.prefix + `throw`))
// {
// 		message.delete();
// 		if (!message.mentions.members.first()) return;
// 		if (message.mentions.users.first().id == message.author.id) return;
// 		const user = await Pranks.findOne({ where: { ownerID: message.author.id } });
// 		if (!user) return;
// 		if (user.bomb == 0) return;
// 		user.decrement('bomb', { by: 1 });
// 		const mentionedUser = message.mentions.members.first();
// 		const i = Math.floor((Math.random() * 2));
// 		await mentionedUser.removeRoles(config.prankcolors)
// 		mentionedUser.addRole(config.prankcolors[i]).catch(console.error);
// 		message.reply('you hurt a Prankster Balloon at your target, covering them with paint. What a trickster!')
// }

else if (message.content.startsWith(config.prefix + `rent`))
{
  message.delete();
  var rentedDays = Math.round(args[1]);
  if (!channelNameRegex.test(args[0]))
    return message.reply('Only alphanumeric characters allowed!');
  if (args[0].length > 30)
    return message.reply('Channel names may be up to 30 characters long!');
  if (isNaN(rentedDays))
    return message.reply('Invalid command syntax.');
  if (rentedDays <= 1)
    return message.reply('Number of days cannot be 1 or under!');
    let date = new Date();
    let hours = date.getHours()  + 1;
    let gemstogive = ((hours/24)*5);
    var daydiscount = Math.round(gemstogive);
    var price = rentedDays * config.rent_cost - daydiscount;
  const userName = await Money.findOne({ where: { userId: message.author.id } });
  if (userName) {
    if (userName.gems < price) return message.reply("Not enough gems!");
    const sentMessage = await message.channel.send(`This will cost ${price} :gem:. Are you sure?`)
      await sentMessage.react('✅');
      await sentMessage.react('❌')
      const filter = (reaction, user) => {
          return user.id === message.author.id
              && (reaction.emoji.name === '✅' || reaction.emoji.name === '❌');
      };
        try{
              const collected = await sentMessage.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] });
                if (collected.first().emoji.name === '❌')
                {
                  sentMessage.delete();
                  return message.channel.send('Transaction cancelled');
                }

              }
              catch (error) {
                sentMessage.delete();
                return message.channel.send('User took too long to reply, transaction canceled.');
              }
              userName.decrement('gems', { by: price });
              sentMessage.delete();
            }
        message.guild.createChannel(args[0], 'text', [
          {
            id: message.author.id,
            allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'MANAGE_MESSAGES'],
          },
          {
            id: message.guild.roles.get(config.modID),
            allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', "MANAGE_MESSAGES"],
          },
          {
            id: message.guild.roles.get(config.uncharted),
            deny: ['VIEW_CHANNEL'],
          },
          {
            id: message.guild.roles.get(config.dyno_role),
            allow: ['VIEW_CHANNEL', 'SEND_MESSAGES']
          },
          {
            id: message.guild.defaultRole,
            deny: ['VIEW_CHANNEL'],
          }]).then(async channel => {
            channel.setParent(config.rent_cat);
            channel.setTopic(`This room has ${rentedDays} days left.`);
            channel.send(config.rented_welcome);
            var logchannel = client.channels.get(config.rent_log);
            logchannel.send(`✅ ${message.author.username} created ${channel.name} for ${rentedDays} days.`)
            try {
              const chnl = await Rent.create({
                ownerID: message.author.id,
                roomID: channel.id,
                days: rentedDays,
              });
              return message.reply(`${channel.toString()} has been created for ${rentedDays} days!`);
    }
    catch (e) {
      return message.reply("Something has gone wrong! Oh noes!");
    }
  });
}



else if (message.content.startsWith(config.prefix + `balance`))

{
  if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID))
  {
  message.delete();
  if (!message.mentions.users.first()) return;
  let argu = message.content.split(" ").slice(1);
  let mentionedUser = message.mentions.users.first().id;
  sql.get(`SELECT * FROM money WHERE userId = '${mentionedUser}'`).then(row => {
    if (!row) return message.reply(`user has no`+ config.currency);
    message.reply(`${argu[0]} has ${row.gems.toFixed(2)}` + config.currency);
  });
 }
}

else if (message.content.startsWith(config.prefix + `give`))
{
  message.delete();
  if (!message.mentions.users.first()) return;
  if (message.member.roles.has(config.role0)) return message.reply("Innocents are unable to use this command.");
  if (message.mentions.members.first().roles.has(config.role0)) return message.reply("You cannnot give gems to Innocents.")
  let argu = message.content.split(" ").slice(1);
  if (!isFinite(parseInt(argu[1], 10))) return;
  var payment;
  payment = parseInt(argu[1], 10);
  var balance;
  let mentionedUser = message.mentions.users.first().id;
  sql.get(`SELECT * FROM money WHERE userId = '${message.author.id}'`).then(row => {
  if (!row) return message.reply("error");
  balance = row.gems;
  if (payment < 0) return message.reply("nope.");
  if (mentionedUser == message.author.id) return message.reply("you cant give gems to yourself!");
  if (balance > payment)
  {
  sql.get(`SELECT * FROM money WHERE userId = '${mentionedUser}'`).then(row => {
    if (!row) return message.reply("error");
    sql.run(`UPDATE money SET gems = ${row.gems + payment} WHERE userId = ${mentionedUser}`);
   });
   sql.get(`SELECT * FROM money WHERE userId = '${message.author.id}'`).then(row => {
     if (!row) return message.reply("error");
     sql.run(`UPDATE money SET gems = ${row.gems - payment} WHERE userId = ${message.author.id}`);
    });
    message.reply("success, " + message.mentions.users.first() + " has been given " + payment + " :gem:");
  }
  else
  {
    message.reply("insuficient funds");
  }
    });
}

else if (message.content.startsWith(config.prefix + `grant`))
{
  message.delete();
  if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID))
  {
  if (!message.mentions.users.first()) return;
  let argu = message.content.split(" ").slice(1);
  if (!isFinite(parseInt(argu[1], 10))) return;
  let grantage = parseInt(argu[1], 10);
  let mentionedUser = message.mentions.users.first().id;
  sql.get(`SELECT * FROM money WHERE userId = '${mentionedUser}'`).then(row => {
    if (!row) return message.reply("error");
    sql.run(`UPDATE money SET gems = ${row.gems + grantage} WHERE userId = ${mentionedUser}`);
   });
   message.reply("the natural order of things has been altered! " + message.mentions.users.first() + " has been granted " + grantage + " " + config.currency + " !");
   message.guild.channels.find("name", "admin_server_log").send("@everyone, " + message.mentions.users.first() + ' has been granted ' + grantage + ' gems by ' + message.author);
 }
}

else
 if (message.content.startsWith(config.prefix + `upgrade`))
 {
   message.delete();
   sql.get(`SELECT * FROM money WHERE userId = '${message.author.id}'`).then(row =>{
       if(message.member.roles.has(config.role8))
       {
         message.reply("you're already at max role, nerd");
       } else if (message.member.roles.has(config.role7))
       {
         if (row.gems > config.upgrade8)
         {
          message.reply("you have reached max role!");
          sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade8} WHERE userId = ${message.author.id}`);
          message.member.addRole(config.role8);
          message.member.removeRole(config.role7);
        }else {
          var difference = config.upgrade8 - row.gems
          message.reply(`not enough! You still need ${difference.toFixed(2)} gems until you can upgrade!`);
        }
       }
       else if (message.member.roles.has(config.role6))
       {
         if (row.gems > config.upgrade7)
         {
         message.reply("transaction successful");
         sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade7} WHERE userId = ${message.author.id}`);
         message.member.addRole(config.role7);
         message.member.removeRole(config.role6);
       } else {
         var difference = config.upgrade7 - row.gems
         message.reply(`not enough! You still need ${difference.toFixed(2)} gems until you can upgrade!`);
       }
       }
       else if (message.member.roles.has(config.role5))
       {
         if (row.gems > config.upgrade6)
         {
         message.reply("transaction successful");
         sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade6} WHERE userId = ${message.author.id}`);
         message.member.addRole(config.role6);
         message.member.removeRole(config.role5);
       } else {
         var difference = config.upgrade6 - row.gems
         message.reply(`not enough! You still need ${difference.toFixed(2)} gems until you can upgrade!`);
       }
       }
       else if (message.member.roles.has(config.role4))
       {
         if (row.gems > config.upgrade5)
         {
         message.reply("transaction successful");
         sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade5} WHERE userId = ${message.author.id}`);
         message.member.addRole(config.role5);
         message.member.removeRole(config.role4);
       } else {
         var difference = config.upgrade5 - row.gems
         message.reply(`not enough! You still need ${difference.toFixed(2)} gems until you can upgrade!`);
       }
       }
       else if (message.member.roles.has(config.role3))
       {
         if (row.gems > config.upgrade4)
         {
         message.reply("transaction successful");
         sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade4} WHERE userId = ${message.author.id}`);
         message.member.addRole(config.role4);
         message.member.removeRole(config.role3);
       } else {
         var difference = config.upgrade4 - row.gems
         message.reply(`not enough! You still need ${difference.toFixed(2)} gems until you can upgrade!`);
       }
       }
       else if (message.member.roles.has(config.role2))
       {
         if (row.gems > config.upgrade3)
         {
         message.reply("transaction successful");
         sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade3} WHERE userId = ${message.author.id}`);
         message.member.addRole(config.role3);
         message.member.removeRole(config.role2);
       } else {
         var difference = config.upgrade3 - row.gems
         message.reply(`not enough! You still need ${difference.toFixed(2)} gems until you can upgrade!`);
       }
       }
       else if (message.member.roles.has(config.role1))
       {
         if (row.gems > config.upgrade2)
         {
         message.reply("transaction successful");
         sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade2} WHERE userId = ${message.author.id}`);
         message.member.addRole(config.role2);
         message.member.removeRole(config.role1);
       } else {
         var difference = config.upgrade2 - row.gems
         message.reply(`not enough! You still need ${difference.toFixed(2)} gems until you can upgrade!`);
       }
       }
       else
       {
         if (row.gems > config.upgrade1)
         {
         message.reply("transaction successful");
         sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade1} WHERE userId = ${message.author.id}`);
         message.member.addRole(config.role1);
         message.member.removeRole(config.role0);
       } else {
         var difference = config.upgrade1 - row.gems
         message.reply(`not enough! You still need ${difference.toFixed(2)} gems until you can upgrade!`);
       }
       }
 });
 }
else return;

});


function clean(text) {
  if (typeof(text) === 'string')
    return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));
  else{
      return text;
}
}

process.on('unhandledRejection', err => console.error(`Uncaught Promise Error: \n ${err.stack}`));
