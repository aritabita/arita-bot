const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json');
const fs = require('fs');
const sql = require('sqlite');
// const Regexp = require('regex');
// const regexp = (`https?://|w{3}\.`);
const cron = require('node-cron');
sql.open('./money.sqlite');

const image = config.imageChannels;
const rp = config.rpChannels;
const varied = config.variedChats;
const general = config.general;
const alt = config.alternate;
// Variables to check the message
const w = 'WWW';
const com = 'COM';
const http = 'HTTP';
const https = 'HTTPS';

client.login(config.token);
client.on('ready', () => {
	console.log(`Arita has been booted up! Arita operates in ${client.guilds.size} servers, and for ${client.users.size} people!`);
});
client.on('error', (e) => console.error(e));
client.on('warn', (e) => console.warn(e));
client.on('debug', (e) => console.info(e));
client.on('ready', () => {
	client.user.setGame('Loli Empress Simulator');
});


client.on('message', (message) =>{
	if (message.channel.type !== 'text') return;
	// split the message up based on a space and put into an array
	const msgArray = message.content.split(' ');
	const args = message.content.split(' ').slice(1);

	if (message.content.startsWith(config.prefix + 'eval')) {
		if(message.author.id !== config.ownerID) return;
		try {
			message.delete();
			const code = args.join(' ');
			let evaled = eval(code);

			if (typeof evaled !== 'string') {evaled = require('util').inspect(evaled);}

			message.channel.send(clean(evaled), { code:'xl' });
		} catch (err) {
			message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
		}
	} else if (message.channel.id == config.main_chat) {
		for(i = 0; i < msgArray.length; i++) { // loop through the array
			const index = msgArray[i].toUpperCase(); // convert the strings to uppercase
			if(index.indexOf(w + '.') !== -1 && index.indexOf('.' + com) !== -1) { // checks to see if the word contains www. and .com

				message.delete();
				message.author.send(`Arita's words echo through your mind..."Hey! ${message.author}! No links allowed in main!"`);
			}
			if(index.indexOf(http) !== -1 || index.indexOf(https) !== -1) { // checks to see if word contains http or https
				if(index.indexOf(w + '.') || index.indexOf('.' + com)) { // checks to see if it contains www. or .com
					message.delete();
					message.author.send(`Arita's words echo through your mind..."Hey! ${message.author}! No links allowed in main!"`);
				}
			}
			// console.log(msgArray[i] + "\n");
		}
		sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
			if (!row) {
				sql.run('INSERT INTO money (userId, gems, exp, expweek, exptoday, expmonth) VALUES (?, ?, ?, ?, ?, ?)', [message.author.id, 1, 1, 1, 1, 1]);
			} else {
				sql.run(`UPDATE money SET gems = ${row.gems + config.general_gain} WHERE userId = ${message.author.id}`);
			}

		}).catch(() => {
			console.error();
			sql.run('CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)').then(() => {
				sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
			});
		});
	} else if (message.content.startsWith(config.prefix + 'lock')) {
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) {
			message.guild.channels.find('name', 'main_chat').overwritePermissions('296324926745608199',
				{
					SEND_MESSAGES: false,
				});
			message.guild.channels.find('name', 'staff_lounge').send('@here SERVER\'S UNDER HEAVY RAID, ALL UNCHARTED USERS HAVE BEEN MUTED. FIRE THE WOOSH BLASTER. SPARE NO ONE!');
		}
	} else if (message.content.startsWith(config.prefix + 'lift')) {
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) {
			message.guild.channels.find('name', 'main_chat').overwritePermissions('296324926745608199',
				{
					SEND_MESSAGES: true,
				});
			message.guild.channels.find('name', 'staff_lounge').send('@here Raid lockdown lifted. Tals ban count still is as high as Mount Everest. Be wary of introductions for the next hour');
		}
	} else if ((image.includes(message.channel.id)) && (message.attachments.size) || (image.includes(message.channel.id)) && (message.content.startsWith('https://'))) {
		sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
			if (!row) {
				sql.run('INSERT INTO money (userId, gems, exp, expweek, exptoday, expmonth) VALUES (?, ?, ?, ?, ?, ?)', [message.author.id, 1, 1, 1, 1, 1]);
			} else {
				sql.run(`UPDATE money SET gems = ${row.gems + config.image_gain} WHERE userId = ${message.author.id}`);
			}

		}).catch(() => {
			console.error();
			sql.run('CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)').then(() => {
				sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
			});
		});
	} else if (rp.includes(message.channel.id) && (!message.content.startsWith('('))) {
		var count = message.content.split(' ').length;
		// sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
		//   if (!row)
		//   {
		//      sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
		//   }
		//   else
		//   {
		//     sql.run(`UPDATE money SET gems = ${row.gems + config.rp_gain + (count * config.rp_word_gain)} WHERE userId = ${message.author.id}`);
		//   }
		sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
			if (!row) {
				sql.run('INSERT INTO money (userId, gems, exp, expweek, exptoday, expmonth) VALUES (?, ?, ?, ?, ?, ?)', [message.author.id, 1, 1, 1, 1, 1]);
			} else {
				sql.run(`UPDATE money SET gems = ${row.gems + config.rp_gain + (count * config.rp_word_gain)} WHERE userId = ${message.author.id}`);
				// sql.run(`UPDATE money SET exp = ${row.exp+ (count * config.exp_gain)} WHERE userId = ${message.author.id}`);
				// sql.run(`UPDATE money SET exptoday = ${row.exptoday+ (count * config.exp_gain)} WHERE userId = ${message.author.id}`);
				// sql.run(`UPDATE money SET expweek = ${row.expweek+ (count * config.exp_gain)} WHERE userId = ${message.author.id}`);
				// sql.run(`UPDATE money SET expmonth = ${row.expmonth+ (count * config.exp_gain)} WHERE userId = ${message.author.id}`);
			}


		}).catch(() => {
			console.error();
			sql.run('CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)').then(() => {
				sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
			});
		});
	} else if (alt.includes(message.channel.id) && (!message.content.startsWith('('))) {
		var count = message.content.split(' ').length;
		// sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
		//   if (!row)
		//   {
		//      sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
		//   }
		//   else
		//   {
		//     sql.run(`UPDATE money SET gems = ${row.gems + config.rp_gain + (count * config.rp_word_gain)} WHERE userId = ${message.author.id}`);
		//   }
		sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
			if (!row) {
				sql.run('INSERT INTO money (userId, gems, exp, expweek, exptoday, expmonth) VALUES (?, ?, ?, ?, ?, ?)', [message.author.id, 1, 1, 1, 1, 1]);
			} else {
				sql.run(`UPDATE money SET gems = ${row.gems + config.rp_gain + (count * config.rp_word_gain)} WHERE userId = ${message.author.id}`);
				// sql.run(`UPDATE money SET exp = ${row.exp+ (count * config.exp_gain)} WHERE userId = ${message.author.id}`);
				// sql.run(`UPDATE money SET exptoday = ${row.exptoday+ (count * config.exp_gain)} WHERE userId = ${message.author.id}`);
				// sql.run(`UPDATE money SET expweek = ${row.expweek+ (count * config.exp_gain)} WHERE userId = ${message.author.id}`);
				// sql.run(`UPDATE money SET expmonth = ${row.expmonth+ (count * config.exp_gain)} WHERE userId = ${message.author.id}`);
			}


		}).catch(() => {
			console.error();
			sql.run('CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)').then(() => {
				sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
			});
		});
	} else if (varied.includes(message.channel.id)) {
		sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
			if (!row) {
				sql.run('INSERT INTO money (userId, gems, exp, expweek, exptoday, expmonth) VALUES (?, ?, ?, ?, ?, ?)', [message.author.id, 1, 1, 1, 1, 1]);
			} else {
				sql.run(`UPDATE money SET gems = ${row.gems + config.varied_gain} WHERE userId = ${message.author.id}`);
			}

		}).catch(() => {
			console.error();
			sql.run('CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)').then(() => {
				sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
			});
		});
	} else if (message.channel.id == config.staff) {
		sql.get(`SELECT * FROM money WHERE userID = '${message.author.id}'`). then(row => {
			if (!row) {
				sql.run('INSERT INTO money (userId, gems, exp, expweek, exptoday, expmonth) VALUES (?, ?, ?, ?, ?, ?)', [message.author.id, 1, 1, 1, 1, 1]);
			} else {
				sql.run(`UPDATE money SET gems = ${row.gems + config.staff_gain} WHERE userId = ${message.author.id}`);
			}

		}).catch(() => {
			console.error();
			sql.run('CREATE TABLE IF NOT EXISTS money (userId TEXT, gems FLOAT)').then(() => {
				sql.run('INSERT INTO money (userId, gems) VALUES (?, ?)', [message.author.id, 1]);
			});
		});
	} else if (!message.content.startsWith(config.prefix) || message.author.bot) {return;} else if (message.content.startsWith(config.prefix + 'money')) {
		message.delete();
		message.reply(' sorry! /money has been depreciated! Please use /status from now on!');
	} else if (message.content.startsWith(config.prefix + 'status')) {
		message.delete();
		sql.get(`SELECT * FROM money WHERE userId = '${message.author.id}'`).then(row => {
			if (!row) return message.reply('you have no' + config.currency);
			message.reply(`you have ${row.gems.toFixed(2)}` + config.currency + ` and ${row.exp.toFixed(3)} experience`);
		});
	} else
	if (message.content.startsWith(config.prefix + 'arita')) {
		message.delete();
		message.channel.send('Arita shall rule forever');
	} else
	// prefix changing
	if (message.content.startsWith(config.prefix + 'prefix')) {
		// Gets prefix from command! (After 'Prefix')
		const newPrefix = message.content.split(' ').slice(1, 2)[0];
		// changes the file!
		config.prefix = newPrefix;
		fs.writeFile('./config.json', JSON.stringify(config));
	} else if (message.content.startsWith(config.prefix + 'deldel')) {
		message.channel.bulkDelete(50);
	} else if (message.content.startsWith(config.prefix + 'balance')) {
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) {
			message.delete();
			if (!message.mentions.users.first()) return;
			const argu = message.content.split(' ').slice(1);
			const mentionedUser = message.mentions.users.first().id;
			sql.get(`SELECT * FROM money WHERE userId = '${mentionedUser}'`).then(row => {
				if (!row) return message.reply('user has no' + config.currency);
				message.reply(`${argu[0]} has ${row.gems.toFixed(2)}` + config.currency);
			});
		}
	} else if (message.content.startsWith(config.prefix + 'give')) {
		message.delete();
		if (!message.mentions.users.first()) return;
		const argu = message.content.split(' ').slice(1);
		if (!isFinite(parseInt(argu[1], 10))) return;
		let payment;
		payment = parseInt(argu[1], 10);
		let balance;
		const mentionedUser = message.mentions.users.first().id;
		sql.get(`SELECT * FROM money WHERE userId = '${message.author.id}'`).then(row => {
			if (!row) return message.reply('error');
			balance = row.gems;
			if (payment < 0) return message.reply('nope.');
			if (mentionedUser == message.author.id) return message.reply('you cant give gems to yourself!');
			if (balance > payment) {
				sql.get(`SELECT * FROM money WHERE userId = '${mentionedUser}'`).then(row => {
					if (!row) return message.reply('error');
					sql.run(`UPDATE money SET gems = ${row.gems + payment} WHERE userId = ${mentionedUser}`);
				});
				sql.get(`SELECT * FROM money WHERE userId = '${message.author.id}'`).then(row => {
					if (!row) return message.reply('error');
					sql.run(`UPDATE money SET gems = ${row.gems - payment} WHERE userId = ${message.author.id}`);
				});
				message.reply('success, ' + message.mentions.users.first() + ' has been given ' + payment + ' :gem:');
			} else {
				message.reply('insuficient funds');
			}
		});
	} else if (message.content.startsWith(config.prefix + 'grant')) {
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) {
			if (!message.mentions.users.first()) return;
			const argu = message.content.split(' ').slice(1);
			if (!isFinite(parseInt(argu[1], 10))) return;
			const grantage = parseInt(argu[1], 10);
			const mentionedUser = message.mentions.users.first().id;
			sql.get(`SELECT * FROM money WHERE userId = '${mentionedUser}'`).then(row => {
				if (!row) return message.reply('error');
				sql.run(`UPDATE money SET gems = ${row.gems + grantage} WHERE userId = ${mentionedUser}`);
			});
			message.reply('the natural order of things has been altered! ' + message.mentions.users.first() + ' has been granted ' + grantage + ' ' + config.currency + ' !');
			message.guild.channels.find('name', 'admin_server_log').send('@everyone, ' + message.mentions.users.first() + ' has been granted ' + grantage + ' gems by ' + message.author);
		}
	} else

	if (message.content.startsWith(config.prefix + 'temp')) {
		const role1Array = message.guild.roles.get(config.role1).members.map(member => member.id);
		for (let i = 0; i < role1Array.length; i++) {
			console.log(role1Array[i]);
			console.log('boop');
			sql.get(`SELECT * FROM money WHERE userId = '${role1Array[i]}'`).then(rows => {
				sql.run(`UPDATE money SET gems = ${rows.gems + config.role1refund} WHERE userId = '${role1Array[i]}'`);
				console.log(role1Array[i]);
			});
		}
	}
	if (message.content.startsWith(config.prefix + 'upgrade')) {
		message.delete();
		sql.get(`SELECT * FROM money WHERE userId = '${message.author.id}'`).then(row =>{
			if(message.member.roles.has(config.role8)) {
				message.reply('you\'re already at max role, nerd');
			} else if (message.member.roles.has(config.role7)) {
				if (row.gems > config.upgrade8) {
					message.reply('you have reached max role!');
					sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade8} WHERE userId = ${message.author.id}`);
					message.member.addRole(config.role8);
					message.member.removeRole(config.role7);
				}else {
					message.reply('you don\'t have enough, nerd');
				}
			} else if (message.member.roles.has(config.role6)) {
				if (row.gems > config.upgrade7) {
					message.reply('transaction successful');
					sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade7} WHERE userId = ${message.author.id}`);
					message.member.addRole(config.role7);
					message.member.removeRole(config.role6);
				} else {
					message.reply('you don\'t have enough, nerd');
				}
			} else if (message.member.roles.has(config.role5)) {
				if (row.gems > config.upgrade6) {
					message.reply('transaction successful');
					sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade6} WHERE userId = ${message.author.id}`);
					message.member.addRole(config.role6);
					message.member.removeRole(config.role5);
				} else {
					message.reply('you don\'t have enough, nerd');
				}
			} else if (message.member.roles.has(config.role4)) {
				if (row.gems > config.upgrade5) {
					message.reply('transaction successful');
					sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade5} WHERE userId = ${message.author.id}`);
					message.member.addRole(config.role5);
					message.member.removeRole(config.role4);
				} else {
					message.reply('you don\'t have enough, nerd');
				}
			} else if (message.member.roles.has(config.role3)) {
				if (row.gems > config.upgrade4) {
					message.reply('transaction successful');
					sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade4} WHERE userId = ${message.author.id}`);
					message.member.addRole(config.role4);
					message.member.removeRole(config.role3);
				} else {
					message.reply('you don\'t have enough, nerd');
				}
			} else if (message.member.roles.has(config.role2)) {
				if (row.gems > config.upgrade3) {
					message.reply('transaction successful');
					sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade3} WHERE userId = ${message.author.id}`);
					message.member.addRole(config.role3);
					message.member.removeRole(config.role2);
				} else {
					message.reply('you don\'t have enough, nerd');
				}
			} else if (message.member.roles.has(config.role1)) {
				if (row.gems > config.upgrade2) {
					message.reply('transaction successful');
					sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade2} WHERE userId = ${message.author.id}`);
					message.member.addRole(config.role2);
					message.member.removeRole(config.role1);
				} else {
					message.reply('you don\'t have enough, nerd');
				}
			} else if (row.gems > config.upgrade1) {
				message.reply('transaction successful');
				sql.run(`UPDATE money SET gems = ${row.gems - config.upgrade1} WHERE userId = ${message.author.id}`);
				message.member.addRole(config.role1);
				message.member.removeRole(config.role0);
			} else {
				message.reply('you don\'t have enough, nerd');
			}
		});
	} else {return;}

});


function clean(text) {
	if (typeof (text) === 'string') {return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));} else{
		return text;
	}
}

// function temprefund()
// {
//   role1Array = message.guild.roles.get(config.role1).members.map(member => member.id);
//   for (i = 0; i < role1Array.length; i++)
// {
//    sql.run(`UPDATE money SET gems = ${row.gems + config.role1refund} WHERE userId = ${role1Array[i]}`);
//    console.log('Done');
// }
// }
function playrefund() {
	role2Array = message.guild.roles.get(config.role2).members.map(member => member.id);
	for (i = 0; i < role2Array.length; i++) {
		sql.run(`UPDATE money SET gems = ${row.gems + config.role2refund} WHERE userId = ${role2Array[i]}`);
		console.log('Done');
	}
}
// function exp(){
//   sql.run(`ALTER TABLE money ADD COLUMN exp FLOAT`);
//   sql.run(`ALTER TABLE money ADD COLUMN exptoday FLOAT`)
//   sql.run(`ALTER TABLE money ADD COLUMN expweek FLOAT`)
//   sql.run(`ALTER TABLE money ADD COLUMN expmonth FLOAT`)
//   sql.run(`UPDATE money SET exptoday = 0.00`)
//   sql.run(`UPDATE money SET expweek = 0.00`);
//   sql.run(`UPDATE money SET expmonth = 0.00`)
//   sql.run(`UPDATE money SET exp = 0.00`)
//   console.log('Done');
// }
//
// var job = cron.schedule('* 0 * * *', function(){
//     console.log('Reset at midnight!');
//     sql.run(`UPDATE money SET exptoday = 0.00`)
// }, true);
// job.start();
//
// var job2 = cron.schedule('* 0 * * 0', function()
// {
//   console.log('Sunday midnight!')
//   sql.run(`UPDATE money SET expweek = 0.00`);
// }, true);
// job2.start();
//
// var job3 = cron.schedule('0 0 0 1 1-12 *', function()
// {
//   console.log('Midnight of the first!')
//   sql.run(`UPDATE money SET expmonth = 0.00`)
// }, true);
// job3.start();
