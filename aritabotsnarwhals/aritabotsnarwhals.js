const Narwhal = database.import('./models/narwhal');

novemberNarwhalEvent(message, msgArray);

async function novemberNarwhalEvent(message, args) {
	if (!message.member.roles.has(config.narwhalEvent)) return;
	let multiplier;
	const teamNarwhal = await Narwhal.findOne({ where: { team: 'narwhal' } });
	const teamFish = await Narwhal.findOne({ where: { team: 'fish' } });
	let messageContent = args.find(obj => obj == '#NarwhalsForever');
	if (!messageContent) {
		messageContent = args.find(obj => obj == 'narwhal');
		if (!messageContent) {
			multiplier = 1;
		}
		else {
			multiplier = 2;
		}
	}
	else {
		multiplier = 3;
	}
	const teamTotal = 0.001 * multiplier;
	teamNarwhal.increment('points', { by: teamTotal });
	teamFish.decrement('points', { by: teamTotal });
}
