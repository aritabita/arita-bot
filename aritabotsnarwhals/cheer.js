const database = require('../database/connect');
const Narwhal = database.import('../models/narwhal');
module.exports = {
	name: 'cheer',
	description: 'Sends a hearthy cheer in support of the Narwhals, grealty appealing to the General Public',
	cooldown: 28800,
	forUser: true,
	aliases: ['che', 'chr'],
	async execute(message) {
		if (message.channel.type !== 'text') return;
		message.delete();
		const teamNarwhal = await Narwhal.findOne({ where: { team: 'narwhal' } });
		const teamFish = await Narwhal.findOne({ where: { team: 'fish' } });
		teamNarwhal.increment('points', { by: 0.05 });
		teamFish.decrement('points', { by: 0.05 });
		message.channel.send(`Closing their eyes, ${message.author.username} gave a resounding cheer, supporting Narwhals everywhere, and appealing greatly to the public! They, however, felt exhausted afterwards.`);
	},
};
