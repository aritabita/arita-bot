const database = require('../database/connect');
const Item = database.import('../models/userItems');
module.exports = {
	name: 'nargive',
	forUser: false,
	cooldown: 5,
	async execute(message) {
		message.delete();
		const narwhal = message.guild.roles.get('511339763572277268');
		const userArray = narwhal.members.keyArray();
		for (let i = 0; i < userArray.length; i++) {
			try {
				await Item.create({
					userId: userArray[i],
					itemKey: 'bnar',
					amount: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
		}
		message.reply('members have obtained item.');
	},
};
