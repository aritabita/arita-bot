const database = require('../database/connect');
const Narwhal = database.import('../models/narwhal');
module.exports = {
	name: 'narwhal',
	description: 'Check the General Public\'s opinion of Narwhals',
	cooldown: 5,
	forUser: true,
	aliases: ['narevent', 'nar', 'narwhals'],
	async execute(message) {
		if (message.channel.type !== 'text') return;
		message.delete();
		const teamNarwhal = await Narwhal.findOne({ where: { team: 'narwhal' } });
		const teamFish = await Narwhal.findOne({ where: { team: 'fish' } });
		// const narwhalTotal = Math.trunc(teamNarwhal.points / 10);
		const fishTotal = Math.trunc(teamFish.points / 10);
		let response = `Public Opinion on Narwhals:\n<:nonarwhal:511730253392117760> ${teamFish.points.toFixed(2)}% `;
		for (let i = 0; i < fishTotal; i++) {
			response += '🔴';
		}
		for (let i = fishTotal; i < 10; i++) {
			response += '🔵';
		}
		response += ` ${teamNarwhal.points.toFixed(2)}% <:narwhal:511730142951768066>\nSupport Narwhals today!`;
		message.reply(response);
	},
};
