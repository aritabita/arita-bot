const config = require('../config');
module.exports = {
	name: 'savenarwhals',
	description: 'Join the resistance against Narwhal Oppresion',
	cooldown: 5,
	forUser: true,
	aliases: ['saven', 'snarwhal'],
	async execute(message) {
		if (message.channel.type !== 'text') return;
		message.delete();
		message.member.roles.add(config.narwhalEvent);
		message.channel.send(`Hooray! ${message.author.username} has joined the movement to support Narwhals! Make us proud!`);
	},
};
