const Sequelize = require('sequelize');
const sequelize = new Sequelize({
	dialect: 'sqlite',
	logging: false,
	operatorsAliases: false,
	// SQLite only
	storage: 'money.sqlite',
});
sequelize.import('models/money');
sequelize.import('models/pranks');
sequelize.import('models/rent');
sequelize.import('models/team');
sequelize.import('models/track');
sequelize.import('models/challenges');
sequelize.import('models/challengeEarns');
sequelize.import('models/candy');
sequelize.import('models/narwhal');
sequelize.import('models/crate');
sequelize.import('models/gift');
sequelize.import('models/purpleGem');
sequelize.import('models/userItems');
sequelize.import('models/boostPotion');
sequelize.import('models/cardInventory');
sequelize.import('models/crate');
sequelize.import('models/anniversaryCurrency');
sequelize.import('models/roomBooster');
sequelize.import('models/waluigi');
sequelize.sync();
