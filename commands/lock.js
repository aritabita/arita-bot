const config = require('../config');
module.exports = {
	name: 'lock',
	description: 'Admin and mod use only. To be used while server is experiencing a heavy raid.',
	forUser: false,
	cooldown: 5,
	execute(message) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) {
			message.client.channels.get('279584314856046592').updateOverwrite('296324926745608199',
				{
					SEND_MESSAGES: false,
				});
			message.client.channels.get('284062124446056459').send('@here SERVER\'S UNDER HEAVY RAID, ALL UNCHARTED USERS HAVE BEEN MUTED. FIRE THE WOOSH BLASTER. SPARE NO ONE!');
		}
	},
};
