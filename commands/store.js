const config = require('../config.json');
const database = require('../database/connect');
const Gift = database.import('../models/gift');
const Item = database.import('../models/userItems');
const PurpleGem = database.import('../models/purpleGem');
const Money = database.import('../models/money');
const Anniversary = database.import('../models/anniversaryCurrency');
module.exports = {
	name: 'store',
	aliases: ['str', 'buy', 'by', 'shop'],
	usage: '<item key>',
	description: 'Checks the store\'s contents, if used with an item key as second argument, it will provide a description of the item, while attempting to use the necessary currency to purchase the item',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return message.channel.send('Really, on their hurry to outdo each other, both merchants have forgotten to set up an online shop.');
		message.delete();
		if (typeof args[0] == 'undefined') {
			let response = 'Glancing about, you see the following shops of note in the Merchant\'s Strip:\n';
			// Loop the different shops based on Owner
			for (let i = 0; i < config.store.length; i++) {
				// Returns if the store is for unlisted items
				if (config.store[i].owner == 'unused') continue;
				// Add owner's name to the message
				response += `**${config.store[i].owner}**\n`;
				// Loop and add each owner's wares through another loop
				for (let o = 0; o < config.store[i].wares.length; o++) {
					response += `${config.store[i].wares[o].emoji}${config.store[i].wares[o].name}[${config.store[i].wares[o].key}]\n`;
				}
			}
			response += 'To let a Merchant know that you want something, simply use this command again with the [key] of the item that you\'d like to purchase.';
			message.reply(response, { split: true });
			return;
		}
		if (args) {
			let item;
			let store;
			// Loops in search for the item in different stores. Set item to the search results if there's a match.
			for (let i = 0; i < config.store.length; i++) {
				const searchResult = config.store[i].wares.find(obj => obj.key == args[0]);
				if (searchResult) {
					store = config.store[i];
					item = searchResult;
				}
			}
			// If item is from the unused store, end command
			if (store.owner == 'unused') return message.reply('never in my life have I heard of such item!');
			// If no match, end command with an error.
			if (!item) return message.reply('never in my life have I heard of such item!');
			// Standard Halloween code. Display item details, give user option to purchase using reactions, and then check if they can afford it.
			const sentMessage = await message.channel.send(`${item.emoji}${item.name}[${item.key}]\n${item.description}\nWould you like to purchase this item?`);
			await sentMessage.react('✅');
			await sentMessage.react('❌');
			const filter = (reaction, user) => {
				return user.id === message.author.id
				&& (reaction.emoji.name === '✅' || reaction.emoji.name === '❌');
			};
			try{
				const collected = await sentMessage.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] });
				if (collected.first().emoji.name === '❌')
				{
					sentMessage.delete();
					return message.channel.send('Window shopping, eh? Fine. Let me know if you want anything else.');
				}
			}
			catch (error) {
				sentMessage.delete();
				return message.channel.send('I got too many costumers to attend! If you can\'t decide then come back later!');
			}
			if (store.currency[0].name == 'Prankster\'s Balloon Mk 2') {
				sentMessage.delete();
				return message.reply('you don\'t have enough of them balloons on ya! Come back when you\'re a little... HMMMMMM richer!');
			}
			if (store.currency[0].name == 'gift') {
				const user = await Gift.findOne({ where: { userId: message.author.id } });
				sentMessage.delete();
				if (!user) return message.reply('you\'re not carrying enough :gift: to pay for that!');
				if (user.gift < item.price) return message.reply('hm. Somehow, I doubt you can pay for this at this moment. Please come when you do.');
				let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.author.id,
							itemKey: item.key,
							amount: 1,
						});
					}
					catch(e) {
						return console.log(e);
					}
					userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
					user.decrement('gift', { by: item.price });
					return message.reply('all done! Thank you for your purchase, and come again!\n**Information on this item has now been added to your status screen**');
				}
				userItem.increment('amount', { by: 1 });
				user.decrement('gift', { by: item.price });
				return message.reply('all done! Thank you for your purchase, and come again!');
			}
			if (store.currency[0].name == 'darkness') {
				const user = await PurpleGem.findOne({ where: { userId: message.author.id } });
				sentMessage.delete();
				if (!user) return message.reply('your soul doesn\'t...seem to be attuned with its darkness yet.');
				if (user.darkness < item.price) return message.reply('you need to collect more darkness! The organization requires it!');
				let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.author.id,
							itemKey: item.key,
							amount: 1,
						});
					}
					catch(e) {
						return console.log(e);
					}
					userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
					user.decrement('darkness', { by: item.price });
					return message.reply('it is done. You might not be who I am looking for, but I still look towards making deals with you again soon all the same.\n**Information on this item has now been added to your status screen**');
				}
				userItem.increment('amount', { by: 1 });
				user.decrement('darkness', { by: item.price });
				return message.reply('it is done. You might not be who I am looking for, but I still look towards making deals with you again soon all the same.');

			}
			if (store.currency[0].name == 'purpleGem') {
				const user = await PurpleGem.findOne({ where: { userId: message.author.id } });
				sentMessage.delete();
				if (!user) return message.reply('your soul doesn\'t...seem to be capable of creating corruption yet.');
				if (user.purpleGem < item.price) return message.reply('I cannot sense enough Purple Gems within you. Come back when you have collected enough.');
				if (item.key == '4lc') {
					let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: 'lc' } });
					if (!userItem) {
						try {
							await Item.create({
								userId: message.author.id,
								itemKey: 'lc',
								amount: 4,
							});
						}
						catch(e) {
							return console.log(e);
						}
						userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
						user.decrement('purpleGem', { by: item.price });
						return message.reply('it is done. May the path of the Darkness be a fruitful one for you.\n**Information on this item has now been added to your status screen**');
					}
					userItem.increment('amount', { by: 4 });
					user.decrement('purpleGem', { by: item.price });
					return message.reply('it is done. May the path of the Darkness be a fruitful one for you.');
				}
				let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.author.id,
							itemKey: item.key,
							amount: 1,
						});
					}
					catch(e) {
						return console.log(e);
					}
					userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
					user.decrement('purpleGem', { by: item.price });
					return message.reply('it is done. May the path of the Darkness be a fruitful one for you.\n**Information on this item has now been added to your status screen**');
				}
				userItem.increment('amount', { by: 1 });
				user.decrement('purpleGem', { by: item.price });
				return message.reply('it is done. May the path of the Darkness be a fruitful one for you.');
			}
			if (store.currency[0].name == 'gem') {
				const user = await Money.findOne({ where: { userId: message.author.id } });
				sentMessage.delete();
				if (!user) return message.reply('I\'m not confident you know what a Gem is.');
				if (user.gems < item.price) return message.reply('I don\'t think you can afford that.');
				let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.author.id,
							itemKey: item.key,
							amount: 1,
						});
					}
					catch(e) {
						return console.log(e);
					}
					userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
					user.decrement('gems', { by: item.price });
					return message.reply('done and done! Enjoy your item!\n**Information on this item has now been added to your status screen**');
				}
				userItem.increment('amount', { by: 1 });
				user.decrement('gems', { by: item.price });
				return message.reply('done and done! Enjoy your item!');
			}
			if (store.currency[0].name == 'bubbles') {
				const user = await Anniversary.findOne({ where: { userId: message.author.id } });
				sentMessage.delete();
				if (!user) return message.reply('you haven\'t even earned any event currency!');
				if (user.bubbles < item.price) return message.reply('you don\'t seem to have enough bubbles on you! Why don\'t you try again when you do!');
				let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.author.id,
							itemKey: item.key,
							amount: 1,
						});
					}
					catch(e) {
						return console.log(e);
					}
					userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
					user.decrement('bubbles', { by: item.price });
					return message.reply('thank you for your purchase! The Cuddlefishes love you!\n**Information on this item has now been added to your status screen**');
				}
				userItem.increment('amount', { by: 1 });
				user.decrement('bubbles', { by: item.price });
				return message.reply('thank you for your purchase! The Cuddlefishes love you!');
			}
			if (store.currency[0].name == 'spinach') {
				const user = await Anniversary.findOne({ where: { userId: message.author.id } });
				sentMessage.delete();
				if (!user) return message.reply('you haven\'t even earned any event currency!');
				if (user.spinach < item.price) return message.reply('oh bother. I don\'t think you can afford this item!');
				let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.author.id,
							itemKey: item.key,
							amount: 1,
						});
					}
					catch(e) {
						return console.log(e);
					}
					userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
					user.decrement('spinach', { by: item.price });
					return message.reply('thank you for the yummy food! I think this is what you wanted.\n**Information on this item has now been added to your status screen**');
				}
				userItem.increment('amount', { by: 1 });
				user.decrement('spinach', { by: item.price });
				return message.reply('thank you for the yummy food! I think this is what you wanted.');
			}
			if (store.currency[0].name == 'tentacles') {
				const user = await Anniversary.findOne({ where: { userId: message.author.id } });
				sentMessage.delete();
				if (!user) return message.reply('you haven\'t even earned any event currency!');
				if (user.tentacles < item.price) return message.reply('far be it for me, letting you sigh forlornly at my wares, no fare for the finery on offer, why don\'t you go farm up some tentacles? See if you can find what attracts them?');
				let item_Key = item.key;
				if (item.key == 'up') item_Key = 'gb';
				if (item.key == 'cp') item_Key = 'lc';
				if (item.key == 'kc') item_Key = 'gmc';
				let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item_Key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.author.id,
							itemKey: item_Key,
							amount: 1,
						});
					}
					catch(e) {
						return console.log(e);
					}
					userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item_Key } });
					user.decrement('tentacles', { by: item.price });
					return message.reply('Placing your purchase into your hands, Est smiles, a gust of wind kicking up as you turn away... And when you look back, the woman is gone.\n**Information on this item has now been added to your status screen. Item might be added under a different key.**');
				}
				userItem.increment('amount', { by: 1 });
				user.decrement('tentacles', { by: item.price });
				return message.reply('Placing your purchase into your hands, Est smiles, a gust of wind kicking up as you turn away... And when you look back, the woman is gone. **Item might have been added under a different key.**');
			}
			if (store.currency[0].name == 'bits') {
				const user = await Anniversary.findOne({ where: { userId: message.author.id } });
				sentMessage.delete();
				if (!user) return message.reply('you haven\'t even earned any event currency!');
				if (user.bits < item.price) return message.reply('ya ain\'t carrying enough of them bits on ya!');
				let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.author.id,
							itemKey: item.key,
							amount: 1,
						});
					}
					catch(e) {
						return console.log(e);
					}
					userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item.key } });
					user.decrement('bits', { by: item.price });
					return message.reply('thank you kindly for your patronage! I hope to see you again!\n**Information on this item has now been added to your status screen**');
				}
				userItem.increment('amount', { by: 1 });
				user.decrement('bits', { by: item.price });
				return message.reply('thank you kindly for your patronage! I hope to see you again!');
			}
			if (store.currency[0].name == 'wahCoins') {
				const user = await Anniversary.findOne({ where: { userId: message.author.id } });
				sentMessage.delete();
				if (!user) return message.reply('you haven\'t even earned any event currency!');
				if (user.wahCoins < item.price) return message.reply('RETURN MESSAGE REPLY ERROR. INSUFFICIENT FUNDS');
				let item_Key = item.key;
				if (item.key == 'tb') item_Key = 'wah';
				if (item.key == 'wh') item_Key = 'in';
				if (item.key == 'in') return message.reply('ERROR ITEM NOT DEFINED. YOU WILL NOT BE CHARGED');
				if (item.key == 'del') item_Key = 'wh';
				if (item.key == 'wah') item_Key = 'del';
				if (item.key == 'undefined') item_Key = 'tb';
				let userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item_Key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.author.id,
							itemKey: item_Key,
							amount: 1,
						});
					}
					catch(e) {
						return console.log(e);
					}
					userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: item_Key } });
					user.decrement('wahCoins', { by: item.price });
					return message.reply('the machine glitches out after taking your payment and spits out an item, wahahahing. You have a suspicion that this is not what you wanted...\n**Information on this item has now been added to your status screen.**');
				}
				userItem.increment('amount', { by: 1 });
				user.decrement('wahCoins', { by: item.price });
				return message.reply('the machine glitches out after taking your payment and spits out an item, wahahahing. You have a suspicion that this is not what you wanted...');
			}
		}
	},
};
