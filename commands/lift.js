const config = require('../config');
module.exports = {
	name: 'lift',
	description: 'Admin and mod use only. Lifts lockdown caused by /lock.',
	forUser: false,
	cooldown: 5,
	execute(message) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID)) {
			message.client.channels.get('279584314856046592').updateOverwrite('296324926745608199',
				{
					SEND_MESSAGES: true,
				});
			message.client.channels.get('284062124446056459').send('@here Raid lockdown lifted. The Death Tally is gigantic. Be wary of introductions for the next hour');
		}
	},
};
