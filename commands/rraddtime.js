const database = require('../database/connect');
const Money = database.import('../models/money');
const config = require('../config');
const Rent = database.import('../models/rent');

module.exports = {
	name: 'rraddtime',
	description: 'Rented Room owner only. Allows the owner of a Room to add time to their room by the selected amount of days.',
	aliases: ['rrat'],
	usage: '<NumberOfDays>',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (!config.rent_cat.includes(message.channel.parentID)) return message.reply('Not a Rented Room.');
		const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
		if (!owner) return;
		if (owner.ownerID != message.author.id) return message.reply('Not the channel owner.');
		const rentedDays = Math.round(args[0]);
		if (isNaN(rentedDays)) return message.reply('Invalid number of days.');
		const user = await Money.findOne({ where: { userId: message.author.id } });
		const price = rentedDays * config.rent_cost;
		if (user) {
			if (user.gems < price) return message.reply('Not enough gems!');
			const sentMessage = await message.channel.send(`This will cost ${price} :gem:. Are you sure?`);
			await sentMessage.react('✅');
			await sentMessage.react('❌');
			const filter = (reaction, IUser) => {
				return IUser.id === message.author.id
				&& (reaction.emoji.name === '✅' || reaction.emoji.name === '❌');
			};
			try{
				const collected = await sentMessage.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] });
				if (collected.first().emoji.name === '❌')
				{
					sentMessage.delete();
					return message.channel.send('Transaction cancelled');
				}
			}
			catch (error) {
				sentMessage.delete();
				return message.channel.send('User took too long to reply, transaction canceled.');
			}
			user.decrement('gems', { by: price });
			sentMessage.delete();
		}
		owner.days = owner.days + parseInt(args[0]);
		await owner.save();
		message.reply(`Days increased by ${rentedDays}!`);
		if (!owner.topic)
		{
			message.channel.setTopic(`This room has ${owner.days} days left.`);
		}
		else {
			message.channel.setTopic(`${owner.topic} || This room has ${owner.days} days left.`);
		}
		const logchannel = message.guild.channels.get(config.rent_log);
		logchannel.send(`💎 ${message.author.username} renewed ${message.channel.name} for ${rentedDays} days.`);
	},
};
