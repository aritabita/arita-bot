const config = require('../config.json');
const database = require('../database/connect');
const Item = database.import('../models/userItems');
const PurpleGem = database.import('../models/purpleGem');
const Money = database.import('../models/money');
const Cards = database.import('../models/cardInventory');
const collectibleCards = require('../collectibleCards');
module.exports = {
	name: 'status',
	aliases: ['st', 'inventory', 'in', 'level', 'lvl'],
	usage: '(n), (cards), (items)',
	description: 'Allows the user to check their current Jolly Experience, alongside their inventory of items from the different stores of the Kinkdom. Adding an n after performing the command will display the experience points in a numerical value instead of graphical',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		// Check if sent in the server or a DM
		if (message.channel.type !== 'text') return message.channel.send('It doesn\'t seem like you can check what you have when you\'re away from your bag!');
		message.delete();
		let response;
		const cardsDatabase = collectibleCards.cards.length;
		if (typeof args[0] == 'undefined') {
			response = `Kinkdom Passport\nMember: ${message.author}\n`;
			// Get the different currencies and add them to display
			const userGems = await Money.findOne({ where: { userId: message.author.id } });
			if (userGems) {
				response += `${userGems.gems.toFixed(2)}${config.currency}, `;
			}
			const userDark = await PurpleGem.findOne({ where: { userId: message.author.id } });
			if (userDark) {
				response += `${userDark.purpleGem} <:purpleGem:522230221328482304>, ${userDark.darkness} <:darkness:522230968551997470>,`;
			}
			response += ' 1 <:PB:430899440451321856>, ';
			// Find each item in its store, with its emoji, based on which are obtained in the item database
			const userItem = await Item.findAll({ where: { userId: message.author.id } });
			if (!userItem) return message.channel.send(response);
			for (let o = 0; o < userItem.length; o++) {
				let item;
				// Loops in search for the item in different stores. Set item to the search results if there's a match.
				for (let i = 0; i < config.store.length; i++) {
					const searchResult = config.store[i].wares.find(obj => obj.key == userItem[o].itemKey);
					if (searchResult) {
						item = searchResult;
					}
				}
				if (!item) continue;
				// Adds item to the inventory
				response += `${userItem[o].amount} ${item.emoji}, `;
			}
			const rand = Math.floor(Math.random() * config.notes.length);
			const notes = config.notes[rand];
			const userCards = await Cards.findAll({ where: { userId: message.author.id } });
			response += '\n';
			if (userCards) {
				response += `Cards obtained: ${userCards.length}/${cardsDatabase}`;
			}
			response += `\nAdditional Notes: ${notes}`;
			message.channel.send(response);
		}
		if (args[0] == 'cards') {
			if (typeof args[1] == 'undefined') {
				response = `${message.author}'s Card Collection:\n`;
				const userCards = await Cards.findAll({ where: { userId: message.author.id } });
				if (userCards == '') {
					response += 'Filled with dust.';
					return message.channel.send(response);
				}
				for (let i = 0; i < userCards.length; i++) {
					const searchResult = collectibleCards.cards.find(obj => obj.key == userCards[i].cardKey);
					if (!searchResult) continue;
					response += `**${userCards[i].amount}** ${searchResult.name}[${searchResult.key}]\n`;
				}
				response += `Total: ${userCards.length}/${cardsDatabase}`;
				return message.author.send(response, { split: true })
					.then(() => message.channel.send('You got your card list in the mail!'))
					.catch(() => message.reply('it seems like I was unable to DM you!'));
			}
			if (args[1] == 'sort') {
				if (typeof args[2] == 'undefined') return message.reply('to sort, you must specify how!');
				response = `${message.author}'s Card Collection:\n`;
				const userCards = await Cards.findAll({ where: { userId: message.author.id } });
				if (userCards == '') {
					response += 'Filled with dust.';
					return message.channel.send(response);
				}
				const cardList = [];
				for (let i = 0; i < userCards.length; i++) {
					const searchResult = collectibleCards.cards.find(obj => obj.key == userCards[i].cardKey);
					if (!searchResult) continue;
					const foundCard = { amount: userCards[i].amount, name: searchResult.name, number: searchResult.number, rarity: searchResult.rarity, key: searchResult.key };
					cardList.push(foundCard);
				}
				if (args[2] == 'alphabetical') {
					cardList.sort(function(a, b) {
						if (a.name < b.name) { return -1; }
						if (a.name > b.name) { return 1; }
						return 0;
					});
					for (let i = 0; i < cardList.length; i++) {
						response += `**${cardList[i].amount}** ${cardList[i].name}[${cardList[i].key}]\n`;
					}
					return message.author.send(response, { split: true })
						.then(() => message.channel.send('You got your card list in the mail!'))
						.catch(() => message.reply('it seems like I was unable to DM you!'));
				}
				if (args[2] == 'number') {
					cardList.sort(function(a, b) {return a.number - b.number;});
					for (let i = 0; i < cardList.length; i++) {
						response += `**${cardList[i].amount}** ${cardList[i].name}[${cardList[i].key}](*#${cardList[i].number}*)\n`;
					}
					return message.author.send(response, { split: true })
						.then(() => message.channel.send('You got your card list in the mail!'))
						.catch(() => message.reply('it seems like I was unable to DM you!'));
				}
				if (args[2] == 'multiples') {
					cardList.sort(function(a, b) {
						if (a.amount < b.amount) { return 1; }
						if (a.amount > b.amount) { return -1; }
						return 0;
					});
					for (let i = 0; i < cardList.length; i++) {
						response += `**${cardList[i].amount}** ${cardList[i].name}[${cardList[i].key}]\n`;
					}
					return message.author.send(response, { split: true })
						.then(() => message.channel.send('You got your card list in the mail!'))
						.catch(() => message.reply('it seems like I was unable to DM you!'));
				}
				if (args[2] == 'rarity') {
					cardList.sort(function(a, b) {
						if (a.rarity < b.rarity) { return -1; }
						if (a.rarity > b.rarity) { return 1; }
						return 0;
					});
					for (let i = 0; i < cardList.length; i++) {
						response += `**${cardList[i].amount}** ${cardList[i].name}[${cardList[i].key}](${cardList[i].rarity})\n`;
					}
					return message.author.send(response, { split: true })
						.then(() => message.channel.send('You got your card list in the mail!'))
						.catch(() => message.reply('it seems like I was unable to DM you!'));
				}
			}
			if (args[1] == 'compare') {
				if (!message.mentions.users.first()) return message.reply('you must say who you\'re comparing with!');
				const userCards = await Cards.findAll({ where: { userId: message.author.id } });
				if (userCards == '') return message.reply('you have no cards yourself!');
				const targetCards = await Cards.findAll({ where: { userId: message.mentions.users.first().id } });
				if (targetCards == '') return message.reply('target has no cards!');
				const userArray = [];
				const targetArray = [];
				for (let i = 0; i < userCards.length; i++) {
					const searchResult = targetCards.find(obj => obj.cardKey == userCards[i].cardKey);
					if (searchResult) continue;
					userArray.push(userCards[i].cardKey);
				}
				for (let i = 0; i < targetCards.length; i++) {
					const searchResult = userCards.find(obj => obj.cardKey == targetCards[i].cardKey);
					if (searchResult) continue;
					targetArray.push(targetCards[i].cardKey);
				}
				response = '__**Cards that you have that target user doesn\'t:**__\n';
				if (userArray == '') {response += 'none.\n';}
				else {
					for (let i = 0; i < userArray.length; i++) {
						const searchResult = collectibleCards.cards.find(obj => obj.key == userArray[i]);
						if (!searchResult) continue;
						response += `${searchResult.name}[${searchResult.key}](${searchResult.rarity})\n`;
					}
				}
				response += `__**Cards that ${message.mentions.users.first()} has that you don't:**__\n`;
				if (targetArray == '') {response += 'none\n';}
				else {
					for (let i = 0; i < targetArray.length; i++) {
						const searchResult = collectibleCards.cards.find(obj => obj.key == targetArray[i]);
						if (!searchResult) continue;
						response += `${searchResult.name}[${searchResult.key}](${searchResult.rarity})\n`;
					}
				}
				message.reply(response, { split: true });
			}
		}
		if (args[0] == 'items') {
			response = `${message.author}'s Detailed Inventory:\n`;
			const userItem = await Item.findAll({ where: { userId: message.author.id } });
			if (!userItem) return message.channel.send(response);
			for (let o = 0; o < userItem.length; o++) {
				let item;
				// Loops in search for the item in different stores. Set item to the search results if there's a match.
				for (let i = 0; i < config.store.length; i++) {
					const searchResult = config.store[i].wares.find(obj => obj.key == userItem[o].itemKey);
					if (searchResult) {
						item = searchResult;
					}
				}
				if (!item) continue;
				// Adds item to the inventory
				response += `${userItem[o].amount} ${item.emoji} ${item.name}[${item.key}]\n`;
			}
			message.channel.send(response);
		}
	},
};
