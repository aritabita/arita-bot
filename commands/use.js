const fs = require('fs');
const Discord = require('discord.js');
const database = require('../database/connect');
const Item = database.import('../models/userItems');
const itemCommands = new Discord.Collection();
const itemFiles = fs.readdirSync('./commands/items').filter(file => file.endsWith('.js'));
for (const file of itemFiles) {
	const command = require(__dirname + `/items/${file}`);
	itemCommands.set(command.name, command);
}
module.exports = {
	name: 'use',
	aliases: ['us'],
	usage: '<key> <user>',
	description: 'Uses purchased item on self or another member.',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return message.channel.send('It doesn\'t seem like you can use what you have when you\'re away from your bag!');
		message.delete();
		if (args == '') return message.reply('You must specify what item!');
		const command = args.shift().toLowerCase();
		if (!itemCommands.has(command)) return message.reply('this item doesn\'t exist.');
		const userItem = await Item.findOne({ where: { userId: message.author.id, itemKey: command } });
		if (!userItem) return message.reply('you don\'t even know what this item is!');
		if (userItem.amount < 1) return message.reply('you don\'t have enough of that item!');
		try {
			itemCommands.get(command).execute(message, userItem, args);
		}
		catch (error) {
			console.error(error);
			message.reply('there was an error trying to use that item!');
		}
	},
};
