const database = require('../database/connect');
const PurpleGem = database.import('../models/purpleGem');
module.exports = {
	name: 'commune',
	description: 'Communes with your inner Darkness, allowing you to attain more. Once you attain 100, you will gain a Purple Gem.',
	cooldown: 28800,
	forUser: true,
	aliases: ['com', 'communion', 'darkness', 'dark'],
	async execute(message) {
		if (message.channel.type !== 'text') return message.channel.send('Try as you might, it doesn\'t seem like your Darkness hears you this far away.');
		message.delete();
		let user = await PurpleGem.findOne({ where: { userID: message.author.id } });
		if (!user) {
			try {
				await PurpleGem.create({
					userID: message.author.id,
					darkness: 0,
					purpleGem: 0,
				});
				user = await PurpleGem.findOne({ where: { userID: message.author.id } });
			}
			catch(e) {
				return console.log(e);
			}
		}
		if (!user) return message.reply('something went wrong!');
		const darkNumber = parseInt(user.darkness, 10);
		const total = darkNumber + 10;
		user.increment('darkness', { by: 10 });
		if (total > 99) {
			user.darkness = 0;
			await user.save();
			user.increment('purpleGem', { by: 1 });
			return message.reply('you commune with your darkness, and upon getting to know your dark side a bit better, you get a Purple Gem.');
		}
		message.reply('you commune with your darkness, and you feel your affinity to it increasing.');
	},
};
