const config = require('../config.json');
const database = require('../database/connect');
const Challenges = database.import('../models/challenges');
const ChallengeEarns = database.import('../models/challengeEarns');
module.exports = {
	name: 'challengelog',
	aliases: ['challenge', 'chalog', 'cl'],
	description: 'Gives user information about their challenge progress',
	forUser: true,
	cooldown: 5,
	async execute(message) {
		if (message.channel.type !== 'text') return message.channel.send('It seems like you\'re unable to find your challenge log here! Maybe you left it at Kinkdom?');
		message.delete();
		let user = await ChallengeEarns.findOne({ where: { userId: message.author.id } });
		if (!user) {
			try {
				await ChallengeEarns.create({
					userId: message.author.id,
					challengeGems: 0,
					challengeClear: 0,
					extendedGems: 0,
					extendedClear: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await ChallengeEarns.findOne({ where: { userId: message.author.id } });
		}
		let response = 'Active Challenges:\n';
		const allChallenges = await Challenges.findAll();
		if (!user) return message.reply('User data is being created. Please try again (To be fixed in a future update)');
		allChallenges.forEach(async activeCat => {
			let challengeInConfig = config.challenges.find(obj => obj.name == activeCat.challengeName);
			let chalGems = user.challengeGems;
			let type = 'main';
			let activeRank;
			let chalName;
			if (!challengeInConfig) {
				challengeInConfig = config.extendedChallenges.find(obj => obj.name == activeCat.challengeName);
				chalGems = user.extendedGems;
				type = 'secondary';
			}
			let rank;
			let reward;
			let total;
			let darkness;
			if (!challengeInConfig) return;
			for (let i = 0; i <= config.role.length; i++) {
				if (!message.member.roles.has(config.role[i])) continue;
				rank = config.upgradePrice[i];
			}
			if (user.challengeClear == 0 && type == 'main' || user.extendedClear == 0 && type == 'secondary')
			{
				chalName = challengeInConfig.rank1;
				activeRank = activeCat.rank1;
				reward = rank * 0.0025;
				total = reward + 5;
				darkness = '25 <:darkness:522230968551997470>';
			}
			if (user.challengeClear == 1 && type == 'main' || user.extendedClear == 1 && type == 'secondary')
			{
				chalName = challengeInConfig.rank2;
				activeRank = activeCat.rank2;
				reward = rank * 0.005;
				total = reward + 10;
				darkness = '50 <:darkness:522230968551997470>';
			}
			if (user.challengeClear == 2 && type == 'main' || user.extendedClear == 2 && type == 'secondary') {
				chalName = challengeInConfig.rank3;
				activeRank = activeCat.rank3;
				reward = rank * 0.01;
				total = reward + 20;
				darkness = '1 <:purpleGem:522230221328482304>';
			}
			if (user.challengeClear == 3 && type == 'main' || user.extendedClear == 3 && type == 'secondary') {
				chalName = challengeInConfig.rank3;
				return response += `${chalName} has been completed!\n`;
			}
			response += `**${chalName}**: ${chalGems.toFixed(2)}💎/${activeRank.toFixed(2)}💎.\nPotential Rewards: ${total.toFixed(2)}💎, ${darkness}\n`;
		});
		response += 'Challenges reset Monday 0:00 EST';
		message.reply(response);
	},
};
