const database = require('../../database/connect');
const config = require('../../config');
const Rent = database.import('../../models/rent');

module.exports = {
	name: 'smc',
	async execute(message, userItem) {
		if (!config.rent_cat.includes(message.channel.parentID)) return message.reply('Not a Rented Room.');
		const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
		if (!owner) return;
		const days = owner.days + 5;
		owner.increment('days', { by: 5 });
		message.reply('using your card, you manage to stabilize this pocket dimension by 5 days! The card, upon use, dissipates.');
		if (!owner.topic)
		{
			message.channel.setTopic(`This room has ${days} days left.`);
		}
		else {
			message.channel.setTopic(`${owner.topic} || This room has ${days} days left.`);
		}
		const logchannel = message.guild.channels.get(config.rent_log);
		logchannel.send(`<:card25:521433176292196352> ${message.author.username} renewed ${message.channel.name} for 5 days using a Silent Gold Tier Membership Card.`);
		userItem.decrement('amount', { by: 1 });
	},
};
