const database = require('../../database/connect');
const Cards = database.import('../../models/cardInventory');

module.exports = {
	name: 'lcarcan',
	async execute(message, userItem) {
		let user = await Cards.findOne({ where: { userId: message.author.id, cardKey: 'arcan' } });
		if (!user) {
			try {
				await Cards.create({
					userId: message.author.id,
					cardKey: 'arcan',
					amount: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Cards.findOne({ where: { userId: message.author.id, cardKey: 'arcan' } });
		}
		user.increment('amount', { by: 1 });
		userItem.destroy();
		message.reply('you obtain the Pumpkin Spice Sea Salt Caramel Strawberry Candy Mint Edition card');
	},
};
