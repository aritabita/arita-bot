const database = require('../../database/connect');
const collectibleCards = require('../../collectibleCards');
const Cards = database.import('../../models/cardInventory');

module.exports = {
	name: 'lc',
	async execute(message, userItem) {
		const rarityNumber = Math.floor(Math.random() * 100);
		let rarity;
		if (rarityNumber < 35) rarity = '⭐';
		else if (rarityNumber < 55) rarity = '⭐⭐';
		else if (rarityNumber < 75) rarity = '⭐⭐⭐';
		else if (rarityNumber < 85) rarity = '⭐⭐⭐⭐';
		else rarity = '⭐⭐⭐⭐⭐';
		const searchedCards = [];
		for (let i = 0; i < collectibleCards.cards.length; i++)
		{
			if (collectibleCards.cards[i].rarity == rarity) searchedCards.push(collectibleCards.cards[i]);
		}
		const randomCard = Math.floor(Math.random() * searchedCards.length);
		const chosenCard = searchedCards[randomCard];
		const userCards = await Cards.findOne({ where: { userId: message.author.id, cardKey: chosenCard.key } });
		if (!userCards) {
			try {
				await Cards.create({
					userId: message.author.id,
					cardKey: chosenCard.key,
					amount: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
			userItem.decrement('amount', { by: 1 });
			return message.reply(`opening the crate, you find ${chosenCard.name}! The card has been added to your collection.\n${chosenCard.description} *${chosenCard.number}* ${chosenCard.rarity}`);
		}
		userCards.increment('amount', { by: 1 });
		userItem.decrement('amount', { by: 1 });
		return message.reply(`opening the crate, you find another ${chosenCard.name}! The card has been added to your inventory.\n${chosenCard.description} *${chosenCard.number}* ${chosenCard.rarity}`);
	},
};
