const database = require('../../database/connect');
const Cards = database.import('../../models/cardInventory');

module.exports = {
	name: 'bnar',
	async execute(message, userItem) {
		try {
			await Cards.create({
				userId: message.author.id,
				cardKey: 'arnarw',
				amount: 2,
			});
		}
		catch(e) {
			return console.log(e);
		}
		try {
			await Cards.create({
				userId: message.author.id,
				cardKey: 'aritch',
				amount: 2,
			});
		}
		catch(e) {
			return console.log(e);
		}
		try {
			await Cards.create({
				userId: message.author.id,
				cardKey: 'arspad',
				amount: 2,
			});
		}
		catch(e) {
			return console.log(e);
		}
		userItem.destroy();
		message.reply('you obtain the Narwhal, Ichthyoid Corp. and Spade cards, as they were taped to the baby narwhal.');
	},
};
