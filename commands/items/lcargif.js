const database = require('../../database/connect');
const Cards = database.import('../../models/cardInventory');

module.exports = {
	name: 'lcargif',
	async execute(message, userItem) {
		let user = await Cards.findOne({ where: { userId: message.author.id, cardKey: 'argif' } });
		if (!user) {
			try {
				await Cards.create({
					userId: message.author.id,
					cardKey: 'argif',
					amount: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Cards.findOne({ where: { userId: message.author.id, cardKey: 'argif' } });
		}
		user.increment('amount', { by: 1 });
		userItem.destroy();
		message.reply('you obtain the Suspiciously Light Gifts card');
	},
};
