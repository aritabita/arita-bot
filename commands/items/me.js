const database = require('../../database/connect');
const Boost = database.import('../../models/boostPotion');
module.exports = {
	name: 'me',
	async execute(message, userItem) {
		let target;
		if (!message.mentions.users.first()) target = message.author.id;
		else target = message.mentions.users.first().id;
		const user = await Boost.findOne({ where: { userId: target } });
		if (user) return message.reply('this potion cannot be used while you\'re experiencing the effects of another!');
		try {
			await Boost.create({
				userId: target,
				type: 'rush',
				multiplier: 0.50,
				time: 30,
			});
		}
		catch(e) {
			return console.log(e);
		}
		message.reply('drinking the elixir, you feel a warm reaction within your body. Gem gains have been increased temporarily!');
		userItem.decrement('amount', { by: 1 });
	},
};
