const database = require('../../database/connect');
const Cards = database.import('../../models/cardInventory');

module.exports = {
	name: 'lcarcuddl',
	async execute(message, userItem) {
		let user = await Cards.findOne({ where: { userId: message.author.id, cardKey: 'arcuddl' } });
		if (!user) {
			try {
				await Cards.create({
					userId: message.author.id,
					cardKey: 'arcuddl',
					amount: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Cards.findOne({ where: { userId: message.author.id, cardKey: 'arcuddl' } });
		}
		user.increment('amount', { by: 1 });
		userItem.destroy();
		message.reply('you obtain the Cuddlefish card');
	},
};
