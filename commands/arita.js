const config = require('../config.json');
module.exports = {
	name: 'arita',
	aliases: ['ari', 'art', 'praise'],
	description: 'Praises Empress',
	forUser: true,
	cooldown: 5,
	execute(message) {
		if (!message.channel.parentID == config.serverEssentials.includes(message.channel.parentID)) return;
		if (message.channel.type !== 'text') return message.channel.send('It doesn\'t seem like Arita can hear you out here...');
		message.delete();
		message.channel.send('Arita shall rule forever');
	},
};
