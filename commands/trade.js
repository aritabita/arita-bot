const database = require('../database/connect');
const collectibleCards = require('../collectibleCards');
const Cards = database.import('../models/cardInventory');
module.exports = {
	name: 'trade',
	aliases: ['trd', 'tr'],
	usage: '<cardToTradeKey> <cardWantedKey> @targetUser',
	description: 'Allows the user to request a Collectible Card trade with another. User must provide: card [key] of the card they offer, card [key] of the card they want, and the user they want to trade with, in that order.',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		// Check if DMs
		if (message.channel.type !== 'text') return message.channel.send('Despite your best attempts, it seems you be in person in the server to perform a trade.');
		message.delete();
		// Check if user has been mentioned
		if (!message.mentions.users.first()) return message.reply('you must mention someone to trade with!');
		// Find the requested cards, return an error if they don't exist
		const cardGiven = collectibleCards.cards.find(obj => obj.key == args[0]);
		const cardWanted = collectibleCards.cards.find(obj => obj.key == args[1]);
		if (!cardGiven || !cardWanted) return message.reply('one of the cards your mention doesn\'t exist!');
		// Find whether or not both user and target user have the requested cards in their inventory
		const userCards = await Cards.findOne({ where: { userId: message.author.id, cardKey: cardGiven.key } });
		if (!userCards || userCards.amount < 1) return message.reply('you don\'t have any of that one to trade!');
		const targetCards = await Cards.findOne({ where: { userId: message.mentions.users.first().id, cardKey: cardWanted.key } });
		if (!targetCards || targetCards.amount < 1) return message.reply('target user doesn\'t have any of those to trade!');
		// Collect responses from target user using a reactions filter, same as Rented Rooms
		const sentMessage = await message.channel.send(`${message.mentions.users.first()}, ${message.author} wants to trade their ${cardGiven.name} for your ${cardWanted.name}, do you accept?`);
		await sentMessage.react('✅');
		await sentMessage.react('❌');
		const filter = (reaction, user) => {
			return user.id === message.mentions.users.first().id
			&& (reaction.emoji.name === '✅' || reaction.emoji.name === '❌');
		};
		try{
			const collected = await sentMessage.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] });
			if (collected.first().emoji.name === '❌')
			{
				sentMessage.delete();
				return message.channel.send('Trade cancelled.');
			}

		}
		catch (error) {
			sentMessage.delete();
			return message.channel.send('Target took too long to reply, trade cancelled.');
		}
		sentMessage.delete();
		// Add the cards to both targets
		let traderCards = await Cards.findOne({ where: { userId: message.author.id, cardKey: cardWanted.key } });
		let tradeeCards = await Cards.findOne({ where: { userId: message.mentions.users.first().id, cardKey: cardGiven.key } });
		if (!traderCards) {
			try {
				await Cards.create({
					userId: message.author.id,
					cardKey: cardWanted.key,
					amount: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			traderCards = await Cards.findOne({ where: { userId: message.author.id, cardKey: cardWanted.key } });
		}
		if (!tradeeCards) {
			try {
				await Cards.create({
					userId: message.mentions.users.first().id,
					cardKey: cardGiven.key,
					amount: 0,
				});
			}
			catch(e) {
				return console.log(e);
			}
			tradeeCards = await Cards.findOne({ where: { userId: message.mentions.users.first().id, cardKey: cardGiven.key } });
		}
		// Increases the traded card and removes on card of the other type from both users.
		traderCards.increment('amount', { by: 1 });
		tradeeCards.increment('amount', { by: 1 });
		if (userCards.amount > 1) {
			userCards.decrement('amount', { by: 1 });
		}
		else {
			userCards.destroy();
		}
		if (targetCards.amount > 1) {
			targetCards.decrement('amount', { by: 1 });
		}
		else {
			targetCards.destroy();
		}
		message.reply(`trade with ${message.mentions.users.first()} completed!`);
	},
};
