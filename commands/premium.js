module.exports = {
	name: 'premium',
	description: 'Enrolls in new paid program using nitro information',
	forUser: true,
	cooldown: 5,
	execute(message) {
		if (message.channel.type !== 'text') return message.channel.send('We only accept payments in person.');
		message.delete();
		message.channel.send('Your soul now belongs to me!');
	},
};
