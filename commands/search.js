const config = require('../config.json');
const cardsDatabase = require('../collectibleCards.json');
module.exports = {
	name: 'search',
	aliases: ['sea', 'src', 'srch'],
	usage: '<type of search> <key>',
	description: 'Allows user to search information on any item or Trading Card available',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return message.channel.send('You can\'t search away from the Kinkdom!');
		message.delete();
		if (typeof args[0] == 'undefined') return message.reply('you must specify a type and an item!');
		if (args[0] == 'card' || args[0] == 'cards') {
			if (typeof args[1] == 'undefined') return message.reply('you must specify what card!');
			const searchResult = cardsDatabase.cards.find(obj => obj.key == args[1]);
			if (!searchResult) return message.reply('provided key doesn\'t match any existing cards.');
			return message.reply(`<:card:517565764375674883> ${searchResult.name}[${searchResult.key}]\n${searchResult.description} *${searchResult.number}* ${searchResult.rarity}`);
		}
		if (args[0] == 'item' || args[0] == 'items') {
			if (typeof args[1] == 'undefined') return message.reply('you must specify what item!');
			let searchResult;
			for (let i = 0; i < config.store.length; i++) {
				const item = config.store[i].wares.find(obj => obj.key == args[1]);
				if (item) {
					searchResult = item;
				}
				continue;
			}
			if (!searchResult) return message.reply('provided key doesn\'t mach any existing items.');
			return message.reply(`${searchResult.emoji}${searchResult.name}[${searchResult.key}]\n${searchResult.description}`);
		}
	},
};
