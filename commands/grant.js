const database = require('../database/connect');
const Money = database.import('../models/money');
const config = require('../config');
module.exports = {
	name: 'grant',
	description: 'Admin and mod use only. Grants the selected amount of gems to the target user',
	usage: '<user> <amount>',
	forUser: false,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return;
		if (!message.channel.parentID == config.serverEssentials.includes(message.channel.parentID)) return;
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID))
		{
			if (!message.mentions.users.first()) return message.reply('you need to mention a user!');
			if (!isFinite(parseInt(args[1], 10))) return message.reply('you need to pass an amount!');
			const grantage = parseInt(args[1], 10);
			const mentionedUser = message.mentions.users.first().id;
			const targetUser = await Money.findOne({ where: { userId: mentionedUser } });
			if (!targetUser) message.reply('empty users cannot be granted gems');
			await targetUser.increment('gems', { by: grantage });
			message.reply(`the natural order of things has been altered! ${message.mentions.users.first()} has been granted ${grantage}${config.currency}!`);
			message.guild.channels.find('name', 'admin_server_log').send(`@everyone, ${message.mentions.users.first()} has been granted ${grantage}${config.currency} by ${message.author}`);
		}
	},
};
