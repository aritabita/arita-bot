module.exports = {
	name: 'signup',
	description: 'Sign up to the Prankster Guild',
	cooldown: 5,
	forUser: false,
	async execute(message) {
		if (message.channel.type !== 'text') return;
		message.delete();
		message.member.roles.add('562276088655642624');
		message.channel.send('There you go! Welcome officially to the Prankster\'s Guild. Make us proud!');
	},
};
