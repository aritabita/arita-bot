const database = require('../database/connect');
const ChallengeEarns = database.import('../models/challengeEarns');
module.exports = {
	name : 'leaderboards',
	aliases: ['leaderboard', 'leaderb', 'lboard', 'lb'],
	description: 'Displays a leaderboard from this week\'s challenge',
	forUser: true,
	cooldown: 5,
	async execute(message) {
		if (message.channel.type == 'text') {
			message.delete();
		}
		const results = await ChallengeEarns.findAll({ limit: 10, order: [['challengeGems', 'DESC']] });
		let response = 'This week\'s Gem Leaderboard!\nRegular Challenge Leaderboard:\n';
		results.forEach(async result => {
			const thisUser = message.client.users.get(result.userId);
			response += `${thisUser.username}: ${result.challengeGems.toFixed(2)}💎\n`;
		});
		response += 'Extended Challenge Leaderboard:\n';
		const extendedResults = await ChallengeEarns.findAll({ limit: 10, order: [['extendedGems', 'DESC']] });
		extendedResults.forEach(async result => {
			const thisUser = message.client.users.get(result.userId);
			response += `${thisUser.username}: ${result.extendedGems.toFixed(2)}💎\n`;
		});
		response += 'Weekly Reset at Monday 0:00 EST';
		message.reply([response], { split: true });
	},
};
