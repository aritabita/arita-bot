const database = require('../database/connect');
const config = require('../config');
const Rent = database.import('../models/rent');

module.exports = {
	name: 'reset',
	aliases: ['rst'],
	description: 'Rented Room owner only. Allows the Owner of a Room to reset it, in order to start a new RP, in a similar fashion as to the manual resets Staff provides.',
	forUser: true,
	cooldown: 5,
	async execute(message) {
		if (message.channel.type !== 'text') return;
		if (message.member.roles.has(config.adminID))
		{
			message.delete();
			const rand = config.resetMessages[Math.floor(Math.random() * config.resetMessages.length)];
			message.channel.send({ embed: {
				color: 3447003,
				description: rand,
			} });
			return;
		}
		message.delete();
		if (!config.rent_cat.includes(message.channel.parentID)) return message.reply('Not a Rented Room.');
		const owner = await Rent.findOne({ where: { roomID: message.channel.id } });
		if (!owner) return;
		if (owner.ownerID != message.author.id) return message.reply('Not the channel owner.');
		message.channel.send({ embed: {
			color: 3447003,
			description: 'This channel has been reset as per the Owner\'s request! \nThe first post after this gets to send the foundation, unless the Owner wishes it otherwise. Anyone who wishes to join in should read through the ongoning roleplay and ask the Owner before doing so.',
		} });
	},
};
