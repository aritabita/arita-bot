const database = require('../database/connect');
const Track = database.import('../models/track');
module.exports = {
	name: 'tracker',
	aliases: ['tra', 'track'],
	description: 'Allows staff to track gem earnings',
	forUser: false,
	cooldown: 5,
	async execute(message) {
		const results = await Track.findAll();
		let response = 'All Time/Weekly Gem Earnings:\n';
		results.forEach(async result => {
			response += `${result.catName}: ${result.gems.toFixed(2)}:gem:/${result.weekGems.toFixed(2)}:gem: this week \n`;
		});
		response += 'Weekly amounts reset Monday 0:00';
		message.reply(response);
	},
};
