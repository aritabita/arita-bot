const Gift = database.import('./models/gift');

else if (message.channel.parentID == '520059872461455370')
{
	const count = message.content.split(' ').length;
	const userName = await Money.findOne({ where: { userId: message.author.id } });
	const moneyTotal = count * config.rp_word_gain;
	if (!userName) {
		try {
			await Money.create({
				userId: message.author.id,
				gems: 1,
			});
		}
		catch(e) {
			return console.log(e);
		}
	}
	else {
		userName.increment('gems', { by: moneyTotal });
	}
	tracker(message, moneyTotal);
	christmas(message, moneyTotal);
	giftDropper(message);
	gemPotion(message, userName, moneyTotal);
	challenge(message, moneyTotal);
}

async function christmas(message, args) {
	const user = await Gift.findOne({ where: { userId: message.author.id } });
	if (!user)
	{
		try {
			await Gift.create({
				userId: message.author.id,
				gift: 0,
				exp: args,
				level: 1,
			});
		}
		catch(e) {
			return console.log(e);
		}
		return;
	}
	if (user.level == 20) return;
	const nextLevel = user.level + 1;
	const earns = args * 5;
	const nextExp = nextLevel * 50;
	const reward = nextLevel * 2;
	const currentExp = user.exp + args;
	await user.increment('exp', { by : earns });
	if (currentExp >= nextExp)
	{
		message.channel.send(`Incredible! You have attained level ${nextLevel}!`);
		await user.increment('gift', { by: reward });
		await user.increment('level', { by: 1 });
		user.exp = 0;
		await user.save();
		// 20 reward goes here
	}
}

async function giftDropper(message) {
	const randomNumber = Math.floor(Math.random() * 101);
	if (randomNumber == 1)
	{
		let user = await Gift.findOne({ where: { userId: message.author.id } });
		if (!user)
		{
			try {
				await Gift.create({
					userId: message.author.id,
					gift: 0,
					exp: 0,
					level: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Gift.findOne({ where: { userId: message.author.id } });
		}
		user.increment('gift', { by: 1 });
		message.author.send('Glancing around, you find a hidden 🎁 laying on the floor nearby.').catch(function() {
			return;
		});
	}
}
