const config = require('../config.json');
const database = require('../database/connect');
const Gift = database.import('../models/gift');
const Item = database.import('../models/userItems');
const PurpleGem = database.import('../models/purpleGem');
const Money = database.import('../models/money');
module.exports = {
	name: 'status',
	aliases: ['st', 'inventory', 'in', 'level', 'lvl'],
	usage: '<n>',
	description: 'Allows the user to check their current Jolly Experience, alongside their inventory of items from the different stores of the Kinkdom. Adding an n after performing the command will display the experience points in a numerical value instead of graphical',
	forUser: true,
	cooldown: 5,
	async execute(message, args) {
		// Check if sent in the server or a DM
		if (message.channel.type !== 'text') return message.channel.send('It doesn\'t seem like you can check what you have when you\'re away from your bag!');
		message.delete();
		// Check if user exists. If not, create user.
		let user = await Gift.findOne({ where: { userId: message.author.id } });
		if (!user) {
			try {
				await Gift.create({
					userId: message.author.id,
					gift: 0,
					exp: 0,
					level: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Gift.findOne({ where: { userId: message.author.id } });
		}
		if (!user) return message.reply('an unexpected error has occurred. Please let Arita know.');
		let response;
		const endlevel = user.level + 1;
		response = `Kinkdom Passport\nMember: ${message.author.username}\nCurrent Jolly Experience:\n`;
		// Decide if experience will be represented with icons or numbers
		if (typeof args[0] == 'undefined') {
			response += `${user.level} `;
			const total = Math.trunc(user.exp / 50);
			for(let i = 0; i < total; i++)
			{
				response += '🎁';
			}
			for (let i = total; i < endlevel; i++)
			{
				response += '🔴';
			}
			response += ` ${endlevel}\nPotential Rewards: ${endlevel * 2} 🎁\n`;
		}
		else if (args != 'n') {return message.reply('that\'s not a valid argument!');}
		else {
			response += `**${user.level}** ${user.exp.toFixed(2)}/${endlevel * 50} **${endlevel}**\nPotential Rewards: ${endlevel * 2} 🎁\n`;
		}
		// Get the different currencies and add them to display
		const userGems = await Money.findOne({ where: { userId: message.author.id } });
		if (userGems) {
			response += `${userGems.gems.toFixed(2)}${config.currency}, `;
		}
		const userDark = await PurpleGem.findOne({ where: { userId: message.author.id } });
		if (userDark) {
			response += `${userDark.purpleGem} <:purpleGem:522230221328482304>, ${userDark.darkness} <:darkness:522230968551997470>,`;
		}
		response += `${user.gift} 🎁, `;
		// Find each item in its store, with its emoji, based on which are obtained in the item database
		const userItem = await Item.findAll({ where: { userId: message.author.id } });
		if (!userItem) return message.channel.send(response);
		for (let o = 0; o < userItem.length; o++) {
			let item;
			// Loops in search for the item in different stores. Set item to the search results if there's a match.
			for (let i = 0; i < config.store.length; i++) {
				const searchResult = config.store[i].wares.find(obj => obj.key == userItem[o].itemKey);
				if (searchResult) {
					item = searchResult;
				}
			}
			// Adds item to the inventory
			response += `${userItem[o].amount} ${item.emoji}, `;
		}
		const rand = Math.floor(Math.random() * config.notes.length);
		const notes = config.notes[rand];
		response += `\nNotes: ${notes}`;
		message.channel.send(response);
	},
};
