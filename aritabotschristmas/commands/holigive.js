const config = require('../config.json');
const database = require('../database/connect');
const Item = database.import('../models/userItems');
const Gift = database.import('../models/gift');
module.exports = {
	name: 'holigive',
	usage: '<item> <amount> <user>',
	description: 'Staff only. Allows for Christmas item granting.',
	forUser: false,
	cooldown: 5,
	async execute(message, args) {
		if (message.channel.type !== 'text') return;
		message.delete();
		if (message.member.roles.has(config.adminID) || message.member.roles.has(config.modID) || message.member.roles.has(config.opID))
		{
			if (!message.mentions.users.first()) return message.reply('I need you to tell me who!');
			if (typeof args[0] == 'undefined') return message.reply('Must specify item!');
			if (args[0]) {
				if (isNaN(args[1])) return message.reply('I need to know how many!');
				let item;
				// Loops in search for the item in different stores. Set item to the search results if there's a match.
				for (let i = 0; i < config.store.length; i++) {
					const searchResult = config.store[i].wares.find(obj => obj.key == args[0]);
					if (searchResult) {
						item = searchResult;
					}
				}
				if (args[0] == 'gift') {
					const userGift = await Gift.findOne({ where: { userId: message.mentions.users.first().id } });
					if (!userGift) return message.reply('error. Tell Arita to check line 30');
					userGift.increment('gift', { by: args[1] });
					return message.reply(`success! ${message.mentions.users.first()} has obtained ${args[1]}🎁`);
				}
				if (!item) return message.reply('Chances are, Ogne made you see this message.');
				const userItem = await Item.findOne({ where: { userId: message.mentions.users.first().id, itemKey: item.key } });
				if (!userItem) {
					try {
						await Item.create({
							userId: message.mentions.users.first().id,
							itemKey: item.key,
							amount: args[1],
						});
						return message.reply(`success! ${message.mentions.users.first()} has obtained ${args[1]}${item.emoji}`);
					}
					catch(e) {
						return console.log(e);
					}
				}
				userItem.increment('amount', { by: args[1] });
				return message.reply(`success! ${message.mentions.users.first()} has obtained ${args[1]}${item.emoji}`);
			}
		}
	},
};
