const database = require('../../database/connect');
const Gift = database.import('../../models/gift');
const Item = database.import('../../models/userItems');
module.exports = {
	name: 'ch',
	async execute(message, userItem) {
		if (!message.mentions.users.first()) return message.reply('your Corruption cannot act without you focusing on a target first!');
		if (message.mentions.users.first().id == message.author.id) return message.reply('this is meant as a curse for others, you cannot use it on yourself!');
		let targetUser = await Gift.findOne({ where: { userId: message.mentions.users.first().id } });
		let user = await Gift.findOne({ where: { userId: message.author.id } });
		if (!targetUser) {
			try {
				await Gift.create({
					userId: message.mentions.users.first().id,
					gift: 0,
					exp: 0,
					level: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
			targetUser = await Gift.findOne({ where: { userId: message.mentions.users.first().id } });
		}
		if (!user) {
			try {
				await Gift.create({
					userId: message.author.id,
					gift: 0,
					exp: 0,
					level: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Gift.findOne({ where: { userId: message.author.id } });
		}
		// SWEATER
		const sweater = await Item.findOne({ where: { userId: message.mentions.users.first().id, itemKey: 'us' } });
		if (sweater && sweater.amount > 0) {
			sweater.decrement('amount', { by: 1 });
			targetUser = user;
			user = await Gift.findOne({ where: { userId: message.mentions.users.first().id } });
			if (targetUser.exp < 10) {
				userItem.decrement('amount', { by: 1 });
				return message.reply('this user doesn\'t have enough experience!');
			}
			message.channel.send('with the power of the Ugly Sweater, the prank went back to its caster!');
		}
		if (targetUser.exp < 10) return message.reply('this user doesn\'t have enough experience!');
		targetUser.decrement('exp', { by: 10 });
		user.increment('exp', { by: 15 });
		userItem.decrement('amount', { by: 1 });
		return message.reply('using your Corruption, you sapped experience from your target');
	},
};
