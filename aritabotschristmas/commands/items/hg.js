const database = require('../../database/connect');
const Money = database.import('../../models/money');
const config = require('../../config');
module.exports = {
	name:'hg',
	async execute(message, userItem) {
		if (!message.mentions.users.first()) return message.reply('the Holiday Gem needs you to think of a target!');
		if (message.mentions.users.first().id == message.author.id) return message.reply('this Gem is meant as a gift to others, it doesn\'t react to you.');
		if (message.mentions.members.first().roles.has(config.role0)) return message.reply('your Gem doesn\'t seem to work on the target...');
		const targetUser = await Money.findOne({ where: { userId: message.mentions.members.first().id } });
		if (!targetUser) return message.reply('something went wrong. Tell Arita to check this item.');
		targetUser.increment('gems', { by: 10 });
		userItem.decrement('amount', { by: 1 });
		message.reply('the Holiday Gem shines brightly and dissipates. The person you thought of felt their wallet a little bit bigger at that moment.');
	},
};
