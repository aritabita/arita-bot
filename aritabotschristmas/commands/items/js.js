const database = require('../../database/connect');
const Gift = database.import('../../models/gift');
module.exports = {
	name: 'js',
	async execute(message, userItem) {
		if (!message.mentions.users.first()) return message.reply('your Shard cannot act without you focusing on a target first!');
		if (message.mentions.users.first().id == message.author.id) return message.reply('this is meant as a boon for others, you cannot use it on yourself!');
		let targetUser = await Gift.findOne({ where: { userId: message.mentions.users.first().id } });
		let user = await Gift.findOne({ where: { userId: message.author.id } });
		if (!targetUser) {
			try {
				await Gift.create({
					userId: message.mentions.users.first().id,
					gift: 0,
					exp: 0,
					level: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
			targetUser = await Gift.findOne({ where: { userId: message.mentions.users.first().id } });
		}
		if (!user) {
			try {
				await Gift.create({
					userId: message.author.id,
					gift: 0,
					exp: 0,
					level: 1,
				});
			}
			catch(e) {
				return console.log(e);
			}
			user = await Gift.findOne({ where: { userId: message.author.id } });
		}
		targetUser.increment('exp', { by: 20 });
		user.increment('exp', { by: 10 });
		userItem.decrement('amount', { by: 1 });
		message.reply('The Shard goes with love to its target, and as they feel its warmth inside, they feel like they\'ve grown more jolly. You feel similarly, watching it happen.');
	},
};
