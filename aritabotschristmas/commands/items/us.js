module.exports = {
	name: 'us',
	async execute(message) {
		return message.reply('this item cannot be used. If you have any, it\'ll trigger automatically. No, even if it\'s ugly, you can\'t take it off!');
	},
};
