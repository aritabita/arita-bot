const config = require('../../config.json');
const database = require('../../database/connect');
const Item = database.import('../../models/userItems');
module.exports = {
	name: 'ps',
	async execute(message, userItem) {
		const number1 = Math.floor(Math.random() * config.adjectives.length);
		const number2 = Math.floor(Math.random() * config.nouns.length);
		const adjective = config.adjectives[number1];
		const noun = config.nouns[number2];
		let targetUser;
		let mentionedUser;
		if (!message.mentions.users.first()) {
			mentionedUser = message.guild.members.get(message.author.id);
		}
		else {
			targetUser = message.mentions.users.first();
			mentionedUser = message.guild.members.get(message.mentions.users.first().id);
			if (mentionedUser.roles.has(config.adminID)) return message.reply('the target was unaffected by your attempts!');
			const sweater = await Item.findOne({ where: { userId:targetUser.id, itemKey: 'us' } });
			if (sweater && sweater.amount > 0) {
				sweater.decrement('amount', { by: 1 });
				mentionedUser = message.guild.members.get(message.author.id);
			}
		}
		mentionedUser.setNickname(`${adjective} ${noun}`);
		const reply = 'As the target takes a deep sniff at this strange potion, they feel as their sense of self gets out of their grasp, a feeling that\'s oddly familiar. They feel very happy.';
		userItem.decrement('amount', { by: 1 });
		mentionedUser.roles.add('526277953332576296');
		message.reply(reply);
	},
};
