const database = require('../../database/connect');
const Money = database.import('../../models/money');
module.exports = {
	name: 'dg',
	async execute(message, userItem) {
		const user = await Money.findOne({ where: { userId: message.author.id } });
		if (!user) return message.reply('error.');
		if (message.mentions.users.first()) return message.reply('this is meant to be used only on yourself.');
		user.increment('gems', { by: 10 });
		message.reply('using the power of your Dark Gem, you suddenly feel your pockets to be heavier.');
		userItem.decrement('amount', { by: 1 });
	},
};
